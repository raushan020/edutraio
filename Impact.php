

<section id="statsSection" class="gradient-bg" style="background-image: url(img/bg-2.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-12">
            <h2 class="heading text-center text-white" data-wow-delay="0.3s">Edutratech CRM</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-1">
                <div class="row">
                    <div class="col-lg-3 col-md-6 mb-3 col-6">
                        <div class="box text-center aos-init aos-animate" data-aos="flip-left" data-aos-duration="800">
                            <img src="img/mainimg/compact/university.png" alt="">
                            <p>Partner <br> Universities</p>
                            <h4>10+</h4>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 mb-3  col-6">
                        <div class="box text-center aos-init aos-animate" data-aos="flip-left" data-aos-duration="1100">
                            <img src="img/mainimg/compact/recruiters-partners.png" alt="">
                            <p>Recruitment <br>Partners</p>
                            <h4>500+</h4>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 mb-3  col-6">
                        <div class="box text-center aos-init aos-animate" data-aos="flip-left" data-aos-duration="1400">
                            <img src="img/mainimg/compact/Applications-per-month.png" alt="">
                            <p>Applications per month</p>
                            <h4>2000+</h4>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 mb-3  col-6">
                        <div class="box text-center aos-init aos-animate" data-aos="flip-left" data-aos-duration="1800">
                            <img src="img/mainimg/compact/Visitors-per-month.png" alt="">
                            <p>Visitors per <br> month</p>
                            <h4>50000+</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>