<section id="advantageSection" class="gradient-bg ">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="heading text-center"> Edutra CRM Advantage</h1>
      </div>
    </div>

    <div class="row align-items-center ">
      <div class="col-md-6">
        <div class="accordion" id="accordionAdvantage">
          <div class="accordion-item">
            <h2 class="accordion-header" id="headingOne">
              <button
                class="accordion-button collapsed"
                onclick="changeAnimatedImg(1)"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#collapseOne"
                aria-expanded="false"
                aria-controls="collapseOne"
              >
                <span>
                  <img
                    src="img/mainimg/uni/team-management.png"
                    class="icon"
                    alt=""
                  /> </span>Team management at your disposal
              </button>
            </h2>
            <div
              id="collapseOne"
              class="accordion-collapse collapse"
              aria-labelledby="headingOne"
              data-bs-parent="#accordionAdvantage"
              style=""
            >
              <div class="accordion-body">
                <p>
                  CRM helps in managing team systematically by arranging all teams, managers, counselors, and current position of leads in a way that it will not create any confusion and makes thing crystal clear and easy to access. </p>
                  <p>Managers of the team are responsible for its whole team and distribute leads to its counselors as per their performance and look into the problems they are facing while counseling. Team meetings and always up for new ideas makes team strong that results in good sales result.  
                </p>
               <!--  <a
                  href="https://uniconnect.leverageedu.com/"
                  target="_blank"
                  rel="noreferrer"
                  class="btn btn-outline-success"
                  >dfrtety</a
                > -->
              </div>
            </div>
          </div>
          <div class="accordion-item">
            <h2 class="accordion-header" id="headingTwo">
              <button
                class="accordion-button collapsed"
                type="button"
                onclick="changeAnimatedImg(2)"
                data-bs-toggle="collapse"
                data-bs-target="#collapseTwo"
                aria-expanded="false"
                aria-controls="collapseTwo"
              >
                <span>
                  <img
                    src="img/mainimg/uni/realationship.png"
                    class="icon"
                    alt=""
                  /> </span
                >A relationship that never fades
              </button>
            </h2>
            <div
              id="collapseTwo"
              class="accordion-collapse collapse"
              aria-labelledby="headingTwo"
              data-bs-parent="#accordionAdvantage"
              style=""
            >
              <div class="accordion-body">
                <p>
                  We will never let you wait or get on the wrong path. With the help of Customer Relationship Management everything and each and every lead handle in a mannered way.</p> 
<p>From fetching the leads through various platforms to instantly starts working on it we do it all. We work as a partner by staying there with you during the whole program. In letting you take the right decision to follow you up throughout the admission process. 

                </p>
                <!-- <a
                  href="https://leverageedu.com/course-finder"
                  target="_blank"
                  rel="noreferrer"
                  class="btn btn-outline-success"
                  >Explore AI Course Finder</a -->
                >
              </div>
            </div>
          </div>
          <div class="accordion-item">
            <h2 class="accordion-header" id="headingThree">
              <button
                class="accordion-button collapsed"
                type="button"
                onclick="changeAnimatedImg(3)"
                data-bs-toggle="collapse"
                data-bs-target="#collapseThree"
                aria-expanded="false"
                aria-controls="collapseThree"
              >
                <span>
                  <img
                    src="img/mainimg/uni/analytics.png"
                    class="icon"
                    alt=""
                /></span>
                Power your sales with analytics
              </button>
            </h2>
            <div
              id="collapseThree"
              class="accordion-collapse collapse"
              aria-labelledby="headingThree"
              data-bs-parent="#accordionAdvantage"
              style=""
            >
              <div class="accordion-body">
                <p>
                  With the sales analytics, you will understand the growth of sales on various platforms and place. It helps the sales team in measuring, managing, and analyzing the sales data and also in making more effective decisions. </p>
<p>CRM allows the organizations to better understanding the insights and managing risk as well as in delivering more accurate sales accurate.  Sales analytics will also work to fill up the gap between efficiency and productivity that occurs during the sales process.</p>

                </p>
              </div>
            </div>
          </div>
          <div class="accordion-item">
            <h2 class="accordion-header" id="headingFour">
              <button
                class="accordion-button collapsed"
                type="button"
                onclick="changeAnimatedImg(4)"
                data-bs-toggle="collapse"
                data-bs-target="#collapseFour"
                aria-expanded="false"
                aria-controls="collapseFour"
              >
                <span>
                  <img
                    src="img/mainimg/uni/basic.png"
                    class="icon"
                    alt=""
                  />
                </span>
                Software that a layman can also understand
              </button>
            </h2>
            <div
              id="collapseFour"
              class="accordion-collapse collapse"
              aria-labelledby="headingFour"
              data-bs-parent="#accordionAdvantage"
              style=""
            >
              <div class="accordion-body">
                <p>
                  To put it simply, User Interface is the most important aspect when considered from a user point of view. The User Interface and User Experience of CRM by Edutratechare seamless and hassle-free. <p>EdutraTech boasts off the ease of accessing the CRM and avail its functionality. The user interface provides smooth integration of interface elements including Input controls, Navigation buttons, and Action Buttons which will provide you the ease of doing the task. </p>
                </p>
              </div>
            </div>
          </div>
          <!-- <div class="accordion-item">
            <h2 class="accordion-header" id="headingFive"> 
              ++




              
              <button
                class="accordion-button collapsed"
                type="button"
                onclick="changeAnimatedImg(5)"
                data-bs-toggle="collapse"
                data-bs-target="#collapseFive"
                aria-expanded="false"
                aria-controls="collapseFive"
              >
                <span>
                  <img
                    src="https://univalley.com/assets/img/team-management.svg"
                    class="icon"
                    alt=""
                  /> </span
                >Full-stack Value Added Services
              </button>
            </h2>
            <div
              id="collapseFive"
              class="accordion-collapse collapse"
              aria-labelledby="headingFive"
              data-bs-parent="#accordionAdvantage"
              style=""
            >
              <div class="accordion-body">
                <p>
                  From loan, scholarships, VISA and forex till the students’
                  arrival at the university campus, we offer the best of
                  post-admission assistance.
                </p>
              </div>
            </div>
          </div> -->
        </div>
      </div>
      <div
        class="col-md-6 mobile-hide aos-init aos-animate"
        data-aos="fade-up"
        data-aos-duration="3000"
      >
        <img
          src="img/mainimg/uni/realtion-ship-image.png"
          id="animationImg"
          class="icon-right"
          alt=""
        />
      </div>
    </div>
  </div>

  <script>
    function changeAnimatedImg(id) {
      console.log("hsajhcbhs");
      
        let image = "img/mainimg/uni/team-management-image.png"
      switch (id) {
        case 1:
          image = "img/mainimg/uni/team-management-image.png";
          break;
        case 2:
          image = "img/mainimg/uni/realtion-ship-image.png";
          break;
        case 3:
          image = "img/mainimg/uni/analytics-image.png";
          break;
        case 4:
          image = "img/mainimg/uni/basic-image.png";
          break;
        // case 5:
        //   image = "https://univalley.com/assets/img/icon4.svg";
        //   break;
      }
      document.getElementById("animationImg").src = image


    }
  </script>
</section>
