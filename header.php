<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Edutratech | Home</title>
    <link rel="icon" href="img/core-img/favicon.ico">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css?v=7847384774">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/anim.css">
    <link rel="stylesheet" href="css/impact.css">
    <link rel="stylesheet" href="css/university.css">
    <link rel="stylesheet" href="css/univalley.css">
    <link rel="stylesheet" href="css/product-tour.css">
    <link rel="stylesheet" href="css/about-s.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    

</head>
<body class="light-version">
    <!-- Preloader -->
    <!-- <div id="preloader">
        <div class="preload-content">
            <div id="dream-load"></div>
        </div>
    </div> -->
<header class="header-area fadeInDown" data-wow-delay="0.2s">
    <div class="classy-nav-container breakpoint-off dark left">
        <div class="container-fluid">
            <!-- Classy Menu -->
            <nav class="classy-navbar justify-content-between" id="dreamNav">

                <!-- Logo -->
                <a class="nav-brand logo-black" href="index.php"><img src="img/mainimg/edu-logo-removebg-preview.png" style="height: 100px" alt="logo" class="header-logo"></a>
                <!-- <a class="nav-brand logo-white" href="index.php"><img src="img/white-footer-logo.png" alt="logo"></a> -->

                <!-- Navbar Toggler -->
                <div class="classy-navbar-toggler">
                    <span class="navbarToggler"><span></span><span></span><span></span></span>
                </div>

                <!-- Menu -->
                <div class="classy-menu">

                    <!-- close btn -->
                    <div class="classycloseIcon">
                        <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                    </div>

                    <!-- Nav Start -->
                    <div class="classynav">
                        <ul id="nav" style="margin: 0">
                           <!--  <li><a href="product-tour.php">Product Tour</a></li>
                            <li><a href="pricing.php">Pricing</a></li>
                            <li><a href="about.php">About</a></li>
                            <li><a href="contact-us.php">Contact</a></li> -->
                          <!--   <li><a href="login.php"  class="margn-lft">Login</a></li> -->
                             <!-- Button -->
                             
                        <a href="login/signup.php" class="btn headerbtn btn-1 btn-clr ml-15 margn-lft">Start Trial</a>
                        </ul>

                       
                    </div>
                    <!-- Nav End -->
                </div>
            </nav>
        </div>
    </div>
</header>