<?php include ('header.php')?>


<style>
section.appoin-form {
    padding-top: 100px;
}

p.topmainP {
    border-top: 1px solid #ddd;
    padding: 18px 0px;
    font-size: 18px;
    color: #424242;
}

.dateapintform .form-row {
    margin-bottom: 25px;
}

.dateapintform .form-row label {
    font-weight: 700;
    color: #525252;
}

.dateapintform li a {
    border: 1px solid #ddd;
    height: 41px;
    display: inline-block;
    line-height: 41px;
    padding: 0px 27px;
    text-decoration: none;
}

ul.d-flex.float-right {
    margin: 0;
}



</style>

<section class="appoin-form">
    <div class="container pt-4 pb-5">
    <div class="row dateapintform">
            <div class="col-sm-6">
                <h3>Confirm your details</h3>
            </div>
            <div class="col-sm-6 text-right">
                <ul class="d-flex float-right">
                    <li><a href="">Appointment</a></li>
                    <li><a href="">Time</a></li>
                    <li><a href="">Confirmation</a></li>
                </ul>
            </div>
            <div class="col-sm-12">
                <p class="topmainP">I want a demo with an Odoo expert - DS - India on <strong>Wed Mar 24, 2021, 3:00:00 PM</strong></p>
            </div>
            <div class="col-sm-8">
                <div class="form-row">
                    <div class="col-sm-3">
                        <label for="Name">Your Name</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-3">
                        <label for="Country">Your Country</label>
                    </div>
                    <div class="col-sm-9">
                        <select class="form-control">
                            <option>---select a country---</option>
                            <option>Afghanistan</option>
                            <option>Afghanistan</option>
                            <option>Afghanistan</option>
                            <option>Afghanistan</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-3">
                        <label for="Email">Your Email</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" name="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-3">
                        <label for="Phone">Your Phone</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="tel" class="form-control" name="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-3">
                        <label for="Company">Company Name</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-3">
                        <label for="Website">Website</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-3">
                        <label for="CompanyS">Company Size</label>
                    </div>
                    <div class="col-sm-9">
                        <select class="form-control">
                            <option>1-5 employees</option>
                            <option>5-15 employees</option>
                            <option>15-50 employees</option>
                            <option>50-100 employees</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-3">
                        <label for="Industry">Your Industry</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-3">
                        <label for="message">Describe your needs:</label>
                    </div>
                    <div class="col-sm-9">
                        <textarea rows="8" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-3">
                        <label for="whos">Who is it for ?</label>
                    </div>
                    <div class="col-sm-9">
                        <div class="radiobox">
                            <label for="company">
                                <input type="radio" id="company" class="" name="" value="company">
                                My company
                            </label>
                        </div>
                        <div class="radiobox">
                            <label for="other">
                                <input type="radio" id="other" class="" name="" value="other">
                                Other
                            </label>
                        </div>
                        <div class="radiobox">
                            <label for="computer">
                                <input type="radio" id="computer" class="" name="" value="computer">
                                One Of My Computer
                            </label>
                        </div>
                        <button class="btn button-new btn-primary mt-3">Confirm Appointment</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include 'footer.php' ?>