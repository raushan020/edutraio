<?php include ('header.php')?>
<section class="gradient-bg mt-10 ">
    <div class="container-flex">
        <div class="breadcrumb-area">
            
                <h1 class="text-4xl font-extrabold text-black sm:text-5xl sm:tracking-tight lg:text-5xl text-center">
        		<span>Privacy-Policy</span>
        		</h1>
        
        </div>
      </div>
</section>
<section>
	<div class="container pt-5 pb-5">
		<div class="row">
			<div class="col-12">
	<p>This Privacy Policy describes our responsibility towards privacy on how we collect, use, maintain, share, and disclose your information, including personal as well as other important information, in conjunction with your access to and use of our website, www.edutratech.com (“Website”).</p>
	<p>When this policy mentions “Edutra LMS,” “we,” “us,” or “our”, it refers to Edutra Technologies LLP, the company that is responsible for your information under this Privacy Policy.
	</p>
	<h4 class="font-weight-bold mb-3">Our Commitment To User(s) Privacy</h4>
	<p>This privacy policy sets out how Edutra LMS uses and protects any information that you give to Edutra LMS when you use this website. We provide the notice explaining our online information practices and the choices you can make about the way your information is collected and used. The privacy policy is available on our homepage for it easy availability and at every point where personally identifiable information may be requested. Edutra LMS may change the privacy policy from time to time by updating the page. The users are requested to check the privacy page from time to time to ensure that you are satisfied with any of the changes made so forth. This policy is effective from October 1, 2019.
	</p>
	<h4 class="font-weight-bold mb-3">The Information We Collect</h4>
	<p>This notice is applicable to all the information or data collected or submitted on the Edutra LMS website. On our contact page, you can make an inquiry, request(s), and register to receive information regarding the services and products we specialize in.
	</p>
	<h4 class="font-weight-bold mb-3">The types of personal information collected at these pages are:</h4>
	<p>Name</p>
	<p>Company</p>
	<p>Email Address</p>
	<p>Phone Number</p>
	<p><strong> By providing this information you approve to be contacted by Edutra LMS.</strong></p>
	<h4 class="font-weight-bold mb-3">The Way We Use Information</h4>
	<p>We make use of the information provided by you while sending an inquiry only to respond to that inquiry. We do not share this information with any outside or third parties except to the extent necessary to answer your inquiry(s). We use return email addresses to answer the emails we receive. Such addresses are not used for any other purpose and are not shared with outside or any third parties. You can register with our website if you would like to receive our newsletter as well as updates on our new products and services. Information you submit on our website will not be used for this purpose unless you fill out the subscription form. We may use the information to customize the website according to your interests. Finally, we never make use of or share the personally identifiable information provided to us online in ways unrelated to the ones described above without also providing you an opportunity to opt-out or otherwise prohibit such unrelated uses.
	</p>
	<h4 class="font-weight-bold mb-3">Our Commitment to Security of Information</h4>
	<p>We are pledged to ensuring that your information is safe and secure with us. To prevent unauthorized access, maintain data accuracy, and ensure the correct use of information, we have placed relevant physical, electronic, and managerial procedures to safeguard and secure the details that we collect online.
	</p>
	<h4 class="font-weight-bold mb-3">Our Cookie Policy</h4>
	<p>A cookie is a small piece of information or data that is sent from a website, whenever an individual accesses a particular website, and stored into an individual’s computer system through the web browser on which that website had been accessed. Cookie asks for permission to store or remember an individual’s record from their previous visit to the website. Once you agree, the data or information is added in the form of a file in your system .Cookies allow customized web pages  and asks for permission to save site login information for the user so that they do not have to login time and again. Websites may use "cookies" to enhance user experience .You can either accept or decline cookies. Most web browsers automatically accept cookies, but you can alter your browser cookie settings according to your preferences. However, disabling cookies may affect some parts of the Website to not function effectively.</p>
	<h4 class="font-weight-bold mb-3">Links to Other Websites</h4>
	<p>Our website may contain links to other pages in order to enable you to visit other websites of interest easily. However, once you choose or click those links to leave our site, you should note that we do not have any control over the other websites. Therefore, we cannot be responsible for the protection and privacy of any information which you provide while visiting such sites and such sites are not governed by this privacy statement.<strong> You should exercise caution and look at the privacy statement applicable to the other website in question.</strong></p>
	<p>We will not sell, distribute or lease your personal information to third parties unless we are permitted to do so by you or are required by law to do so. We may only use your personal data to send you promotional information about third parties which we think you may find interesting if you tell us that you wish this to happen.</p>
	<p>In case you wish to contact us regarding any questions, inquiries or concerns about these privacy policies, break the ice at:<strong> support@edutratech.com</strong></p>
	</div>
	</div>
	</div>
</section>
<?php include ('footer.php')?>