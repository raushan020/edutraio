<?php include ('header.php')?>


<style>
section.demo-secd {
    padding-top: 100px;
}

.date-picker {
  width: 100%;
  height: auto;
  max-height: 400px;
  background: white;
  position: relative;
  overflow: hidden;
  transition: all 0.3s 0s ease-in-out;
}
.date-picker .input {
  width: 100%;
  height: 50px;
  font-size: 0;
  cursor: pointer;
}
.date-picker .input .result, .date-picker .input button {
  display: inline-block;
  vertical-align: top;
}

.date-picker .input .result {
    width: calc(100% - 50px);
    height: 50px;
    line-height: 50px;
    font-size: 16px;
    padding: 0 10px;
    color: #086ad8;
    box-sizing: border-box;
    border: 1px solid #ddd;
    font-weight: 600;
    text-align: center;
    text-transform: uppercase;
}

.date-picker .input button {
  width: 50px;
  height: 50px;
  background-color: #8392A7;
  color: white;
  line-height: 50px;
  border: 0;
  font-size: 18px;
  padding: 0;
}
.date-picker .input button:hover {
  background-color: #68768A;
}
.date-picker .input button:focus {
  outline: 0;
}
.date-picker .calendar {
  position: relative;
  width: 100%;
  background: #fff;
  border-radius: 0px;
  overflow: hidden;
}
.date-picker .ui-datepicker-inline {
  position: relative;
  width: 100%;
}
.date-picker .ui-datepicker-header {
  height: 100%;
  line-height: 50px;
  background: #086ad8;
  color: #fff;
  margin-bottom: 10px;
}
.date-picker .ui-datepicker-prev, .date-picker .ui-datepicker-next {
  width: 20px;
  height: 20px;
  text-indent: 9999px;
  border: 2px solid #fff;
  border-radius: 100%;
  cursor: pointer;
  overflow: hidden;
  margin-top: 12px;
  line-height: 43px;
}
.date-picker .ui-datepicker-prev {
  float: left;
  margin-left: 12px;
}
.date-picker .ui-datepicker-prev:after {
  transform: rotate(45deg);
  margin: -43px 0px 0px 8px;
}
.date-picker .ui-datepicker-next {
  float: right;
  margin-right: 12px;
}
.date-picker .ui-datepicker-next:after {
  transform: rotate(-135deg);
  margin: -43px 0px 0px 6px;
}
.date-picker .ui-datepicker-prev:after, .date-picker .ui-datepicker-next:after {
  content: "";
  position: absolute;
  display: block;
  width: 4px;
  height: 4px;
  border-left: 2px solid #fff;
  border-bottom: 2px solid #fff;
}
.date-picker .ui-datepicker-prev:hover, .date-picker .ui-datepicker-next:hover, .date-picker .ui-datepicker-prev:hover:after, .date-picker .ui-datepicker-next:hover:after {
  border-color: #68768A;
}
.date-picker .ui-datepicker-title {
  text-align: center;
}
.date-picker .ui-datepicker-calendar {
  width: 100%;
  text-align: center;
}
.date-picker .ui-datepicker-calendar thead tr th span {
  display: block;
  width: 100%;
  color: #8392A7;
  margin-bottom: 5px;
  font-size: 13px;
}
.date-picker .ui-state-default {
  display: block;
  text-decoration: none;
  color: #b5b5b5;
  line-height: 40px;
  font-size: 12px;
}

.date-picker .ui-state-default:hover {
    background: rgb(215 223 239 / 86%);
    color: #606b79;
}

.date-picker .ui-state-highlight {
  color: #68768A;
}
.date-picker .ui-state-active {
    color: #ffffff;
    background-color: rgb(8 106 216);
    font-weight: 600;
}
.date-picker .ui-datepicker-unselectable .ui-state-default {
  color: #eee;
  border: 2px solid transparent;
}
.date-picker.open {
  max-height: 50px;
}
.date-picker.open .input button {
  background: #68768A;
}

.time-dateappoint a:hover {
    background: #086ad8;
    color: #fff;
}

.time-dateappoint a {
    color: #000;
    font-size: 16px;
    display: block;
    line-height: 37px;
    font-weight: 600;
    text-align: center;
    text-decoration: none;
}
.time-dateappoint {
    position: absolute;
    top: 0;
    width: 137px;
    background: #fff;
    padding: 0px;
    box-shadow:0px 7px 11px #0000001a;
    display: none;
}

p.topmainP {
    border-top: 1px solid #ddd;
    padding: 18px 0px;
}

.breadecstm li a {
    border: 1px solid #ddd;
    height: 41px;
    display: inline-block;
    line-height: 41px;
    padding: 0px 27px;
    text-decoration: none;
}

ul.d-flex.float-right {
    margin: 0;
}
.breadecstm {
    margin-bottom: 41px;
    border-bottom: 1px solid #ddd;
}

.time-dateappoint.fsttime {
    left: -120px;
}

.time-dateappoint.nexttime {
    right: -120px;
}

</style>

<section class="demo-secd">
<div class="datepickermai container pt-5 pb-5">
	<div class="row">
    <div class="col-sm-12 ">
        <div class="breadecstm">
            <div class="row">
            <div class="col-sm-6">
			<h3>Appointment Time</h3>
		</div>
		<div class="col-sm-6 text-right">
			<ul class="d-flex float-right">
				<li><a href="">Appointment</a></li>
				<li><a href="">Time</a></li>
				<li><a href="">Confirmation</a></li>
			</ul>
		</div>
            </div>
        </div>
    </div>
		<div class="col-sm-6">
			<div class="date-picker datafst">
				<div class="input">
					<div class="result">Select Date <span></span></div>
					<button><i class="fa fa-calendar"></i></button>
				</div>
				<div class="calendar"></div>
			</div>
			<div class="time-dateappoint fsttime">
				
                <a href="appointment.php">09:00</a>
                <a href="appointment.php">09:30</a>
                <a href="appointment.php">10:00</a>
                <a href="appointment.php">10:30</a>
                <a href="appointment.php">11:00</a>
                <a href="appointment.php">12:30</a>
                <a href="appointment.php">13:00</a>
                <a href="appointment.php">13:30</a>
                <a href="appointment.php">14:00</a>
                <a href="appointment.php">15:00</a>
                <a href="appointment.php">15:30</a>
                
			</div>
		</div>
		<div class="col-sm-6">
			<div class="date-picker datanext">
				<div class="input">
					<div class="result">Select Date <span></span></div>
					<button><i class="fa fa-calendar"></i></button>
				</div>
				<div class="calendar"></div>
			</div>
			<div class="time-dateappoint nexttime">
				
                <a href="appointment.php">09:00</a>
                <a href="appointment.php">09:30</a>
                <a href="appointment.php">10:00</a>
                <a href="appointment.php">10:30</a>
                <a href="appointment.php">11:00</a>
                <a href="appointment.php">12:30</a>
                <a href="appointment.php">13:00</a>
                <a href="appointment.php">13:30</a>
                <a href="appointment.php">14:00</a>
                <a href="appointment.php">15:00</a>
                <a href="appointment.php">15:30</a>
                
			</div>
		</div>
	</div>
</div>
</section>


<?php include 'footer.php' ?>