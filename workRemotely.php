<link rel="stylesheet" href="css/style.css">
<section>
<div class="container">
    <div class="section-heading text-center mt-5">
        <h2 class="d-blue bold fadeInUp " data-wow-delay="0.3s">WORK REMOTELY WITH</h2>
        <p class="fadeInUp" data-wow-delay="0.4s">Lorem Ipsum is simply dummy text of the printing and
            typesetting industry</p>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <div class="row region-promo2 d-flex justify-content-center align-items-center">
                <div class="col-3 d-flex justify-content-center flex-column align-items-center">
                    <img src="img/notes.png" class="apps-image mr-2" />
                    <h5 class="mt-4">Remotely</h5>
                </div>
                <div class="col-9">
                    <h5>Your home-office toolkit</h5>
                    <p style="font-size: 80%">
                        A suite of web and mobile applications designed for distributed teams.
                    </p>

                    <button type="button" class="btn btn-danger btn-sm rounded-0">SIGN UP NOW</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="row" style="min-height: 341.984px !important;">

                <div class="col-6 main-apps1 d-flex">
                    <img src="img/notes.png" class="apps-image mr-2" />
                    <div class="">
                        <h6>Assist</h6>
                        <p class="apps-p">Support remote customers instantly.</p>
                        <button type="button" class="btn btn-outline-primary btn-sm">
                            Primary
                        </button>
                    </div>
                </div>

                <div class="col-6 main-apps2 d-flex">
                    <img src="img/notes.png" class="apps-image mr-2" />
                    <div class="">
                        <h6>Meeting</h6>
                        <p class="apps-p">Meeting &amp; webinar solution.</p>
                        <button type="button" class="btn btn-outline-primary btn-sm">
                            Primary
                        </button>
                    </div>
                </div>


                <div class="col-6 main-apps3 d-flex">
                    <img src="img/notes.png" class="apps-image mr-2" />
                    <div class="">
                        <h6>Work Drive</h6>
                        <p class="apps-p">Secure business email.</p>
                        <button type="button" class="btn btn-outline-primary btn-sm">
                            Primary
                        </button>
                    </div>
                </div>


                <div class="col-6 main-apps4 d-flex">
                    <img src="img/notes.png" class="apps-image mr-2" />
                    <div class="">
                        <h6>clip</h6>
                        <p class="apps-p">Secure business email.</p>
                        <button type="button" class="btn btn-outline-primary btn-sm">
                            Primary
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>
</section>