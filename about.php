<?php include ('header.php') ?>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css">


<style type="">
   
.timeline-carousel {
  padding: 86px 6.9444% 90px 6.9444%;
  position: relative;
  overflow: hidden;
      background: #1d1d1e;
}
.timeline-carousel:after, .timeline-carousel:before {
  content: "";
  position: absolute;
  display: block;
  top: 0;
  height: 100%;
  width: 6.9444%;
  background-color: #1d1d1e;
  z-index: 3;
  width: 6.9444%;
}
.timeline-carousel:after {
  left: 0;
}
.timeline-carousel:before {
  right: 0;
  opacity: 0;
}
.timeline-carousel .slick-list {
  overflow: visible;
}
.timeline-carousel .slick-dots {
  bottom: -73px;
}
.timeline-carousel h1 {
  color: white;
  font-size: 46px;
  line-height: 50pd;
  margin-bottom: 40px;
  font-weight: 900;
}
.timeline-carousel__image {
  padding-right: 30px;
}
.timeline-carousel__item {
  cursor: pointer;
}
.timeline-carousel__item .media-wrapper {
  opacity: 0.4;
  padding-bottom: 71.4%;
  -webkit-transition: all 0.4s cubic-bezier(0.55, 0.085, 0.68, 0.53);
  -o-transition: all 0.4s cubic-bezier(0.55, 0.085, 0.68, 0.53);
  transition: all 0.4s cubic-bezier(0.55, 0.085, 0.68, 0.53);
}
.timeline-carousel__item:last-child .timeline-carousel__item-inner:after {
  width: calc(100% - 30px);
}
.timeline-carousel__item-inner {
  position: relative;
  padding-top: 45px;
}
.timeline-carousel__item-inner:after {
  position: absolute;
  width: 100%;
  top: 45px;
  left: 0;
  content: "";
  border-bottom: 1px solid rgba(255, 255, 255, 0.2);
}
.timeline-carousel__item-inner .year {
  font-size: 36px;
  line-height: 36px;
  color: white;
  display: table;
  letter-spacing: -1px;
  padding-right: 10px;
  background-color: #1d1d1e;
  z-index: 1;
  position: relative;
  margin: -15px 0 20px;
  font-weight: 900;
}
.timeline-carousel__item-inner .year:after {
  content: "";
  position: absolute;
  display: block;
  left: -10px;
  top: 0;
  height: 100%;
  width: 10px;
  background-color: #1d1d1e;
  z-index: 3;
}
.timeline-carousel__item-inner .month {
  font-size: 12px;
  text-transform: uppercase;
  color: #bd4f70;
  display: block;
  margin-bottom: 10px;
  font-weight: 900;
}
.timeline-carousel__item-inner p {
  font-size: 12px;
  line-height: 18px;
  color: white;
  width: 60%;
  font-weight: 400;
  margin-bottom: 15px;
}
.timeline-carousel__item-inner .read-more {
  font-size: 12px;
  color: #bd4f70;
  display: table;
  margin-bottom: 10px;
  font-weight: 900;
  text-decoration: none;
  position: relative;
}
.timeline-carousel__item-inner .read-more:after {
  content: "";
  position: absolute;
  left: 0;
  bottom: -1px;
  width: 0;
  border-bottom: 2px solid #bd4f70;
  -webkit-transition: all 0.2s cubic-bezier(0.55, 0.085, 0.68, 0.53);
  -o-transition: all 0.2s cubic-bezier(0.55, 0.085, 0.68, 0.53);
  transition: all 0.2s cubic-bezier(0.55, 0.085, 0.68, 0.53);
}
.timeline-carousel__item-inner .read-more:hover:after {
  width: 100%;
}
.timeline-carousel__item-inner .pointer {
  height: 29px;
  position: relative;
  z-index: 1;
  margin: -4px 0 16px;
}
.timeline-carousel__item-inner .pointer:after, .timeline-carousel__item-inner .pointer:before {
  position: absolute;
  content: "";
}
.timeline-carousel__item-inner .pointer:after {
  width: 9px;
  height: 9px;
  border-radius: 100%;
  top: 0;
  left: 0;
  background-color: #bd4f70;
}
.timeline-carousel__item-inner .pointer:before {
  width: 1px;
  height: 100%;
  top: 0;
  left: 4px;
  background-color: #bd4f70;
}
.timeline-carousel .slick-active .media-wrapper {
  opacity: 1 !important;
}

.slick-dots {
  bottom: 60px;
  list-style: none;
  position: absolute;
  width: 100%;
  left: 0;
  text-align: center;
  z-index: 2;
}
.slick-dots li {
  cursor: pointer;
  display: inline-block;
  margin: 0 6px;
  position: relative;
  width: 10px;
  height: 10px;
}
.slick-dots li:last-child {
  margin-right: 0;
}
.slick-dots li.slick-active button {
  background: #bd4f70;
  border-color: #bd4f70;
}
.slick-dots li button {
  display: block;
  font-size: 0;
  width: 10px;
  height: 10px;
  padding: 0;
  background-color: rgba(255, 255, 255, 0.6);
  border-color: rgba(255, 255, 255, 0.6);
  cursor: pointer;
  -webkit-transition: all 0.4s cubic-bezier(0.55, 0.085, 0.68, 0.53);
  -o-transition: all 0.4s cubic-bezier(0.55, 0.085, 0.68, 0.53);
  transition: all 0.4s cubic-bezier(0.55, 0.085, 0.68, 0.53);
}
.slick-dots li button:hover {
  background: #bd4f70;
  border-color: #bd4f70;
}

.link {
  position: absolute;
  left: 0;
  bottom: 0;
  padding: 20px;
  z-index: 9999;
}
.link a {
  display: flex;
  align-items: center;
  text-decoration: none;
  color: #fff;
}
.link .fa {
  font-size: 28px;
  margin-right: 8px;
  color: #fff;
}
</style>
 <section class="gradient-bg mt-10 ">
    <div class="container-flex">
        <div class="breadcrumb-area">
            
                <h1 class="text-4xl font-extrabold text-black sm:text-5xl sm:tracking-tight lg:text-5xl text-center">
                    <span>About-Us</span></h1>
        
        </div>
      </div>
    </section>
<section class="about-us-area trial-bg-1 section-padding-200 clearfix">
    <div class="overlay-1"></div>
</section>
<section >
	<div class="row h-100 align-items-center justify-content-center">
	<div class="col-md-8 col-lg-8 col-sm-12 about-div">
    <div class="row text-align">

		<h2 class="font-medium">Strategic Implementation of Software for Unifying customers and the Companies </h2>

		<p class="text-base">Edutratech was launched in 2012 to address the most strategic challenge that businesses face: how to manage their customers and team together. Since then, we are instilling the ideals we hold close to - Innovation, accountability, 
      and Service – in our software and reimagining how companies should think about sales, along with the expertise and teamwork at the forefront.</p>
		</div>
	</div>
	</div>
</section>

<!-- This example requires Tailwind CSS v2.0+ -->
<div class="relative bg-white py-16 sm:py-4">
  <div class="lg:mx-auto lg:max-w-7xl lg:px-8 lg:grid lg:grid-cols-2 lg:gap-24 lg:items-start">
    <div class="relative sm:py-16 lg:py-0">
      <div aria-hidden="true" class="hidden sm:block lg:absolute lg:inset-y-0 lg:right-0 lg:w-screen">
        <div class="absolute inset-y-0 right-1/2 w-full bg-gray-50 rounded-r-3xl lg:right-72"></div>
        <svg class="absolute top-8 left-1/2 -ml-3 lg:-right-8 lg:left-auto lg:top-12" width="404" height="392" fill="none" viewBox="0 0 404 392">
          <defs>
            <pattern id="02f20b47-fd69-4224-a62a-4c9de5c763f7" x="0" y="0" width="20" height="20" patternUnits="userSpaceOnUse">
              <rect x="0" y="0" width="4" height="4" class="text-gray-200" fill="currentColor" />
            </pattern>
          </defs>
          <rect width="404" height="392" fill="url(#02f20b47-fd69-4224-a62a-4c9de5c763f7)" />
        </svg>
      </div>
      <div class="relative mx-auto max-w-md px-4 sm:max-w-3xl sm:px-6 lg:px-0 lg:max-w-none lg:py-20">
        <!-- Testimonial card-->
        <div class="relative pt-64 pb-10 rounded-2xl shadow-xl overflow-hidden">
          <img class="absolute inset-0 h-full w-full object-cover" src="https://images.unsplash.com/photo-1521510895919-46920266ddb3?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&fp-x=0.5&fp-y=0.6&fp-z=3&width=1440&height=1440&sat=-100" alt="">
          <div class="absolute inset-0 bg-indigo-500" style="mix-blend-mode: multiply;"></div>
          <div class="absolute inset-0 bg-gradient-to-t from-indigo-600 via-indigo-600 opacity-90"></div>
          <div class="relative px-8">
            <div>
              <img class="h-12" src="https://tailwindui.com/img/logos/workcation.svg?color=white" alt="Workcation">
            </div>
            <blockquote class="mt-8">
              <div class="relative text-lg font-medium text-white md:flex-grow">
                <svg class="absolute top-0 left-0 transform -translate-x-3 -translate-y-2 h-8 w-8 text-indigo-400" fill="currentColor" viewBox="0 0 32 32" aria-hidden="true">
                  <path d="M9.352 4C4.456 7.456 1 13.12 1 19.36c0 5.088 3.072 8.064 6.624 8.064 3.36 0 5.856-2.688 5.856-5.856 0-3.168-2.208-5.472-5.088-5.472-.576 0-1.344.096-1.536.192.48-3.264 3.552-7.104 6.624-9.024L9.352 4zm16.512 0c-4.8 3.456-8.256 9.12-8.256 15.36 0 5.088 3.072 8.064 6.624 8.064 3.264 0 5.856-2.688 5.856-5.856 0-3.168-2.304-5.472-5.184-5.472-.576 0-1.248.096-1.44.192.48-3.264 3.456-7.104 6.528-9.024L25.864 4z" />
                </svg>
                <p class="relative text-white">
                  Tincidunt integer commodo, cursus etiam aliquam neque, et. Consectetur pretium in volutpat, diam. Montes, magna cursus nulla feugiat dignissim id lobortis amet.
                </p>
              </div>

              <footer class="mt-4">
                <p class="text-base font-semibold text-indigo-200 text-white">Sarah Williams, CEO at Workcation</p>
              </footer>
            </blockquote>
          </div>
        </div>
      </div>
    </div>

    <div class="relative mx-auto max-w-md px-4 sm:max-w-3xl sm:px-6 lg:px-0">
      <!-- Content area -->
      <div class="pt-12 sm:pt-16 lg:pt-20">
        <h2 class="text-3xl text-gray-900 font-extrabold tracking-tight sm:text-4xl">
        Journey This Far 
        </h2>
        <div class="mt-6 text-gray-500 space-y-6">
          <p class="text-lg">
          As we started growing from who we were to what we wanted to be, we unintentionally started developing our employee brand and shaping our culture. Was it ever about technological ability? Isn't that something that can be learned? Rather, we concentrated on people's inherent nature.
          </p>
          <p class="text-base leading-7">
          We evaluated each prospective employee based on their personality, emotional skillset, and tendencies.
Nobody can lead a business to succeed on its own. Success is built on the collective achievement of many people. We all lead because of the hard work of everyone on the team who has worked, contributed, and committed to the same clearly stated goals.
          </p>
          <p class="text-base leading-7">
          Empowering our teams begins with an understanding of the pool of collective excellence in our organization, which eventually empowers our mission. And we recall the trust and confidence that almost all of us received early in our careers from leaders who saw our potential and gave us the power to achieve more.
          </p>
        </div>
      </div>

      <!-- Stats section -->
      <div class="mt-4">
        <dl class="grid grid-cols-2 gap-x-4 gap-y-8">
          <div class="border-t-2 border-gray-100 pt-6">
            <dt class="text-base font-medium text-gray-500">Founded</dt>
            <dd class="text-3xl font-extrabold tracking-tight text-gray-900">2021</dd>
          </div>

          <div class="border-t-2 border-gray-100 pt-6">
            <dt class="text-base font-medium text-gray-500">Employees</dt>
            <dd class="text-3xl font-extrabold tracking-tight text-gray-900">5</dd>
          </div>

          <div class="border-t-2 border-gray-100 pt-6">
            <dt class="text-base font-medium text-gray-500">Beta Users</dt>
            <dd class="text-3xl font-extrabold tracking-tight text-gray-900">521</dd>
          </div>

          <div class="border-t-2 border-gray-100 pt-6">
            <dt class="text-base font-medium text-gray-500">Raised</dt>
            <dd class="text-3xl font-extrabold tracking-tight text-gray-900">$25M</dd>
          </div>
        </dl>
        <div class="mt-4">
          <a href="#" class="text-base font-medium text-indigo-600"> Learn more about how we're changing the world <span aria-hidden="true">&rarr;</span> </a>
        </div>
      </div>
    </div>
  </div>
</div>

  

    <section class="clients_testimonials_area bg-img section-padding-50-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 justify-content-center text-center">
                        <h2 class="d-blue bold bl-text wow fadeInUp animated">Our Mission</h2>
                        <p style="font-size: 1.125rem;
    line-height: 1.875rem;
}">There's a common misconception that in order to expand a company,
 you must be stone cold and harsh. We know, however, that there is a better way to expand.
  One in which what is good for the bottom line is also good for the clients. 
  To do this, we have established a culture that supports our team members, so they deliver outstanding products and can provide exceptional service to our customers. We have designed a fully functional ecosystem that combines software and service to assist companies in growing.</p>
                    </div>
                     <div class="col-md-6">
                         <img src="https://www.hubspot.com/hubfs/assets/hubspot.com/about_2019/BrianAndDharmesh-photo.png">
                     </div>
                </div>
            </div>
        </section>
  
     <!-- This example requires Tailwind CSS v2.0+ -->
<div class="bg-white">
  <div class="max-w-7xl mx-auto py-12 px-4 text-center sm:px-6 lg:px-8 lg:py-24">
    <div class="space-y-8 sm:space-y-12">
      <div class="space-y-5 sm:mx-auto sm:max-w-xl sm:space-y-4 lg:max-w-5xl">
        <h2 class="text-3xl font-extrabold tracking-tight sm:text-4xl">The People</h2>
        <p class="text-xl text-gray-500">Meet the Team that has been pulling it off behind the closed doors and sleepless nights. </p>
      </div>
      <ul  class="mx-auto grid grid-cols-2 gap-x-4 gap-y-8 sm:grid-cols-4 md:gap-x-6 lg:max-w-5xl lg:gap-x-8 lg:gap-y-12 xl:grid-cols-4" x-max="1">
        <li>
          <div class="space-y-4">
           <img class="mx-auto h-20 w-20 rounded-full lg:w-24 lg:h-24" src="https://images.unsplash.com/photo-1519244703995-f4e0f30006d5?ixlib=rb-=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=facearea&amp;facepad=8&amp;w=1024&amp;h=1024&amp;q=80" alt="">
            <div class="space-y-2">
              <div class="text-xs font-medium lg:text-sm">
                <h6>Michael Foster</h6>
                <p class="text-indigo-600">Co-Founder / CTO</p>
              </div>
            </div>
          </div>
        </li>
         <li>
          <div class="space-y-4">
            <img class="mx-auto h-20 w-20 rounded-full lg:w-24 lg:h-24" src="https://images.unsplash.com/photo-1519244703995-f4e0f30006d5?ixlib=rb-=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=8&w=1024&h=1024&q=80" alt="">
            <div class="space-y-2">
              <div class="text-xs font-medium lg:text-sm">
                <h6>Michael Foster</h6>
                <p class="text-indigo-600">Co-Founder / CTO</p>
              </div>
            </div>
          </div>
        </li>
         <li>
          <div class="space-y-4">
            <img class="mx-auto h-20 w-20 rounded-full lg:w-24 lg:h-24" src="https://images.unsplash.com/photo-1519244703995-f4e0f30006d5?ixlib=rb-=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=8&w=1024&h=1024&q=80" alt="">
            <div class="space-y-2">
              <div class="text-xs font-medium lg:text-sm">
                <h6>Michael Foster</h6>
                <p class="text-indigo-600">Co-Founder / CTO</p>
              </div>
            </div>
          </div>
        </li>
         <li>
          <div class="space-y-4">
            <img class="mx-auto h-20 w-20 rounded-full lg:w-24 lg:h-24" src="https://images.unsplash.com/photo-1519244703995-f4e0f30006d5?ixlib=rb-=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=8&w=1024&h=1024&q=80" alt="">
            <div class="space-y-2">
              <div class="text-xs font-medium lg:text-sm">
                <h6>Michael Foster</h6>
                <p class="text-indigo-600">Co-Founder / CTO</p>
              </div>
            </div>
          </div>
        </li>



        <!-- More items... -->
      </ul>
    </div>
  </div>
</div>

<!--Timeline carousel-->
<section class="timeline-carousel">
   <h1>Our Journey So Far...</h1>
   <div class="timeline-carousel__item-wrapper" data-js="timeline-carousel">
      <!--Timeline item-->
      <div class="timeline-carousel__item">
         <div class="timeline-carousel__image">
            <div class="media-wrapper media-wrapper--overlay" style="background: url('https://live.staticflickr.com/65535/49293320037_6789b13bb3_w.jpg') center center; background-size:cover;"></div>
         </div>
         <div class="timeline-carousel__item-inner">
            <span class="year">November 2013</span>

            <p>The idea for Moditions is born. Christine begins work on logo, product range and all base legal and compliance operations.</p>

         </div>
      </div>
      <!--/Timeline item-->

      <!--Timeline item-->
      <div class="timeline-carousel__item">
         <div class="timeline-carousel__image">
            <div class="media-wrapper media-wrapper--overlay" style="background: url('http://factsforkids.net/wp-content/uploads/2013/09/13.jpg') center center; background-size:cover;"></div>
         </div>
         <div class="timeline-carousel__item-inner">
            <div class="pointer"></div>
            <span class="month">July 5  </span>
            <p>Austria-Hungary seeks German support for a war against Serbia in case of Russian militarism. Germany gives assurances of support.</p>
            <a href="#" class="read-more">Read more</a>
         </div>
      </div>
      <!--/Timeline item-->

      <!--Timeline item-->
      <div class="timeline-carousel__item">
         <div class="timeline-carousel__image">
            <div class="media-wrapper media-wrapper--overlay" style="background: url('https://cdn-images-1.medium.com/max/2000/1*tjpdoOeFp6PfczMjqh6JEA.jpeg') center center; background-size:cover;"></div>
         </div>
         <div class="timeline-carousel__item-inner">
            <span class="year">1915</span>
            <span class="month">January 2</span>
            <p>The Russian offensive in the Carpathians begins. It will continue until April 12. </p>
            <a href="#" class="read-more">Read more</a>
         </div>
      </div>
      <!--/Timeline item-->

      <!--Timeline item-->
      <div class="timeline-carousel__item">
         <div class="timeline-carousel__image">
            <div class="media-wrapper media-wrapper--overlay" style="background: url('https://ichef-1.bbci.co.uk/news/660/media/images/72349000/jpg/_72349652_generals-on-horseback.jpg') center center; background-size:cover;"></div>
         </div>
         <div class="timeline-carousel__item-inner">
            <div class="pointer"></div>
            <span class="month">January 18–19 </span>
            <p>Battle of Jassin. </p>
            <a href="#" class="read-more">Read more</a>
         </div>
      </div>
      <!--/Timeline item-->

      <!--Timeline item-->
      <div class="timeline-carousel__item">
         <div class="timeline-carousel__image">
            <div class="media-wrapper media-wrapper--overlay" style="background: url('https://thechive.files.wordpress.com/2014/05/world-war-1-photography-30.jpg?quality=85&strip=info&w=550') center center; background-size:cover;"></div>
         </div>
         <div class="timeline-carousel__item-inner">
            <span class="year">1916</span>
            <span class="month">January 5–17</span>
            <p>Austro-Hungarian offensive against Montenegro, which capitulates. </p>
            <a href="#" class="read-more">Read more</a>
         </div>
      </div>
      <!--/Timeline item-->

      <!--Timeline item-->
      <div class="timeline-carousel__item">
         <div class="timeline-carousel__image">
            <div class="media-wrapper media-wrapper--overlay" style="background: url('https://kidskonnect.com/wp-content/uploads/2010/10/Approaching_Omaha.jpg') center center; background-size:cover;"></div>
         </div>
         <div class="timeline-carousel__item-inner">
            <div class="pointer"></div>
            <span class="month">January 6–7 </span>
            <p>Battle of Mojkovac. </p>
            <a href="#" class="read-more">Read more</a>
         </div>
      </div>
      <!--/Timeline item-->
   </div>
</section>
<!--Timeline carousel-->


  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.js"></script>

<script type="">
  $.js = function (el) {
    return $('[data-js=' + el + ']')
};

function carousel() {
  $.js('timeline-carousel').slick({
    infinite: false,
    arrows: false,
    dots: true,
    autoplay: false,
    speed: 1100,
    slidesToShow: 3,
    slidesToScroll: 2,
    responsive: [
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }]
  });
}

carousel();
</script>


<?php include ('footer.php')?>