<?php include ('header.php')?>
<style>
.trial-bg-2 {
    
    background-size: cover;
    background: #086ad8;
}
.classynav ul li a {
    
    color: #000 !important;
    
}
</style>
<section class="gradient-bg mt-10 ">
    <div class="container-flex">
         <div class="breadcrumb-area">
            
                <h1 class="text-4xl font-extrabold text-black sm:text-5xl sm:tracking-tight lg:text-5xl text-center">
                    <span>Pricing</span></h1>
        
        </div>
      </div>
    </section>
    <!-- This example requires Tailwind CSS v2.0+ -->
<div class="bg-gray-900">
  <div class="pt-12 px-4 sm:px-6 lg:px-8 lg:pt-10">
    <div class="text-center">
      
      <p class="mt-2 text-3xl font-extrabold text-white sm:text-4xl lg:text-5xl">
      Such a Little Price to pay 
      </p>
      <p class="mt-3 max-w-4xl mx-auto text-xl text-white sm:mt-5 sm:text-2xl">
      for Your Streamline Business. Customize the most suitable plans according to Business Size.
      </p>
    </div>
  </div>

  <div class="mt-16 bg-white pb-12 lg:mt-5 lg:pb-20">
    <div class="relative z-0">
      <div class="absolute inset-0 h-5/6 bg-gray-900 lg:h-2/3"></div>
      <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="relative lg:grid lg:grid-cols-7">
          <div class="max-w-md lg:max-w-none lg:col-start-1 lg:col-end-3 lg:row-start-2 lg:row-end-3">
            <div class="h-full flex flex-col rounded-lg shadow-lg overflow-hidden lg:rounded-none lg:rounded-l-lg">
              <div class="flex-1 flex flex-col">
                <div class="bg-white px-6 py-10">
                  <div>
                    <h3 class="text-center text-2xl font-medium text-gray-900" id="tier-hobby">
                    Basic
                    </h3>
                    <div class="mt-4 flex items-center justify-center">
                      <span class="px-3 flex items-start text-6xl tracking-tight text-gray-900">
                        <span class="mt-2 mr-2 text-4xl font-medium">
                        Rs.
                        </span>
                        <span class="font-bold">
                         0
                        </span>
                      </span>
                      <span class="text-xl font-medium text-gray-500">
                        /month
                      </span>
                    </div>
                  </div>
                </div>
                <div class="flex-1 flex flex-col justify-between border-t-2 border-gray-100 p-6 bg-gray-50 sm:p-10 lg:p-6 xl:p-10">
                  <ul class="space-y-4">
                    <li class="flex items-start">
                      <div class="flex-shrink-0">
                        <!-- Heroicon name: outline/check -->
                        <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                        </svg>
                      </div>
                      <p class="ml-3 text-base font-medium text-gray-500">
                      Lead Management
                      </p>
                    </li>

                    <li class="flex items-start">
                      <div class="flex-shrink-0">
                        <!-- Heroicon name: outline/check -->
                        <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                        </svg>
                      </div>
                      <p class="ml-3 text-base font-medium text-gray-500">
                        Sales Management
                      </p>
                    </li>

                    <li class="flex items-start">
                      <div class="flex-shrink-0">
                        <!-- Heroicon name: outline/check -->
                        <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                        </svg>
                      </div>
                      <p class="ml-3 text-base font-medium text-gray-500">
                        Reports
                      </p>
                    </li>

                    <li class="flex items-start">
                      <div class="flex-shrink-0">
                        <!-- Heroicon name: outline/check -->
                        <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                        </svg>
                      </div>
                      <p class="ml-3 text-base font-medium text-gray-500">
                      Team Collaboration upto
                      </p>
                    </li>

                    <li class="flex items-start">
                      <div class="flex-shrink-0">
                        <!-- Heroicon name: outline/check -->
                        <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                        </svg>
                      </div>
                      <p class="ml-3 text-base font-medium text-gray-500">
                      Data Security
                      </p>
                    </li>

                    <li class="flex items-start">
                      <div class="flex-shrink-0">
                        <!-- Heroicon name: outline/check -->
                        <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                        </svg>
                      </div>
                      <p class="ml-3 text-base font-medium text-gray-500">
                      System Maintenance
                      </p>
                    </li>

                    <li class="flex items-start">
                      <div class="flex-shrink-0">
                        <!-- Heroicon name: outline/check -->
                        <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                        </svg>
                      </div>
                      <p class="ml-3 text-base font-medium text-gray-500">
                      Technical Support
                      </p>
                    </li>
                  </ul>
                  <div class="mt-3">
                    <div class="rounded-lg shadow-md">
                      <a href="#" class="block w-full text-center rounded-lg border border-transparent bg-white px-6 py-3 text-base font-medium text-indigo-600 hover:bg-gray-50" aria-describedby="tier-hobby">
                        Start your trial
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="mt-15 max-w-lg lg:max-w-none lg:mx-0 lg:col-start-3 lg:col-end-6 lg:row-start-1 lg:row-end-4">
            <div class="relative z-10 rounded-lg shadow-xl">
              <div class="pointer-events-none absolute inset-0 rounded-lg border-2 border-indigo-600" aria-hidden="true"></div>
              <div class="absolute inset-x-0 top-0 transform translate-y-px">
                <div class="flex justify-center transform -translate-y-1/2">
                  <span class="inline-flex rounded-full bg-indigo-600 px-4 py-1 text-sm font-semibold tracking-wider uppercase text-white">
                    Most popular
                  </span>
                </div>
              </div>
              <div class="bg-white rounded-t-lg px-6 pt-12 pb-10">
                <div>
                  <h3 class="text-center text-3xl font-semibold text-gray-900 sm:-mx-6" id="tier-growth">
                    Essential
                  </h3>
                  <div class="mt-4 flex items-center justify-center">
                    <span class="px-3 flex items-start text-6xl tracking-tight text-gray-900 sm:text-6xl">
                      <span class="mt-2 mr-2 text-4xl font-medium">
                      Rs.
                      </span>
                      <span class="font-bold">
                        149
                      </span>
                    </span>
                    <span class="text-2xl font-medium text-gray-500">
                      /user/month
                    </span>
                  </div>
                </div>
              </div>
              <div class="border-t-2 border-gray-100 rounded-b-lg pt-10 pb-8 px-6 bg-gray-50 sm:px-10 sm:py-10">
                <ul class="space-y-4">
                  <li class="flex">
                    <div class="flex-shrink-0">
                      <!-- Heroicon name: outline/check -->
                      <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                      </svg>
                    </div>
                    <p class="ml-3 text-base font-medium text-gray-500">
                    All in Basic, Plus
                    </p>
                  </li>

                  <li class="flex">
                    <div class="flex-shrink-0">
                      <!-- Heroicon name: outline/check -->
                      <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                      </svg>
                    </div>
                    <p class="ml-3 text-base font-medium text-gray-500">
                      Advanced Lead Management
                    </p>
                  </li>

                  <li class="flex">
                    <div class="flex-shrink-0">
                      <!-- Heroicon name: outline/check -->
                      <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                      </svg>
                    </div>
                    <p class="ml-3 text-base font-medium text-gray-500">
                    Advanced Sales Management
                    </p>
                  </li>

                  <li class="flex">
                    <div class="flex-shrink-0">
                      <!-- Heroicon name: outline/check -->
                      <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                      </svg>
                    </div>
                    <p class="ml-3 text-base font-medium text-gray-500">
                    Advanced Report
                    </p>
                  </li>

                  <li class="flex">
                    <div class="flex-shrink-0">
                      <!-- Heroicon name: outline/check -->
                      <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                      </svg>
                    </div>
                    <p class="ml-3 text-base font-medium text-gray-500">
                    Analytics Measurement
                    </p>
                  </li>

                  <li class="flex">
                    <div class="flex-shrink-0">
                      <!-- Heroicon name: outline/check -->
                      <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                      </svg>
                    </div>
                    <p class="ml-3 text-base font-medium text-gray-500">
                    Marketing Automation
                    </p>
                  </li>

                  <li class="flex">
                    <div class="flex-shrink-0">
                      <!-- Heroicon name: outline/check -->
                      <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                      </svg>
                    </div>
                    <p class="ml-3 text-base font-medium text-gray-500">
                    Multiple user
                    </p>
                  </li>

                  <li class="flex">
                    <div class="flex-shrink-0">
                      <!-- Heroicon name: outline/check -->
                      <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                      </svg>
                    </div>
                    <p class="ml-3 text-base font-medium text-gray-500">
                    Advanced Customized Report
                    </p>
                  </li>

                  <li class="flex">
                    <div class="flex-shrink-0">
                      <!-- Heroicon name: outline/check -->
                      <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                      </svg>
                    </div>
                    <p class="ml-3 text-base font-medium text-gray-500">
                    Team Collaboration upto
                    </p>
                  </li>
                </ul>
                <div class="mt-3">
                  <div class="rounded-lg shadow-md">
                    <a href="#" class="block w-full text-center rounded-lg border border-transparent bg-indigo-600 px-6 py-3 text-xl leading-6 font-medium text-white hover:bg-indigo-700" aria-describedby="tier-growth">
                      Start your trial
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="max-w-md lg:max-w-none lg:col-start-6 lg:col-end-8 lg:row-start-2 lg:row-end-3">
            <div class="h-full flex flex-col rounded-lg shadow-lg overflow-hidden lg:rounded-none lg:rounded-r-lg">
              <div class="flex-1 flex flex-col">
                <div class="bg-white px-6 py-10">
                  <div>
                    <h3 class="text-center text-2xl font-medium text-gray-900" id="tier-scale">
                      Pro
                    </h3>
                    <div class="mt-4 flex items-center justify-center">
                      <span class="px-3 flex text-6xl tracking-tight text-gray-900">
                        <span class="mt-2 mr-2 text-4xl font-medium">
                         Rs.
                        </span>
                        <span class="font-bold">
                          349
                        </span>
                      </span>
                      <span class="text-xl font-medium text-gray-500">
                        /month
                      </span>
                    </div>
                  </div>
                </div>
                <div class="flex-1 flex flex-col justify-between border-t-2 border-gray-100 p-6 bg-gray-50 sm:p-10 lg:p-6 xl:p-10">
                  <ul class="space-y-4">
                    <li class="flex items-start">
                      <div class="flex-shrink-0">
                        <!-- Heroicon name: outline/check -->
                        <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                        </svg>
                      </div>
                      <p class="ml-3 text-base font-medium text-gray-500">
                      All in Essential, Plus
                      </p>
                    </li>

                    <li class="flex items-start">
                      <div class="flex-shrink-0">
                        <!-- Heroicon name: outline/check -->
                        <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                        </svg>
                      </div>
                      <p class="ml-3 text-base font-medium text-gray-500">
                        Advance Data Security
                      </p>
                    </li>

                    <li class="flex items-start">
                      <div class="flex-shrink-0">
                        <!-- Heroicon name: outline/check -->
                        <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                        </svg>
                      </div>
                      <p class="ml-3 text-base font-medium text-gray-500">
                       Team Collaboration upto
                      </p>
                    </li>
                    <li class="flex items-start">
                      <div class="flex-shrink-0">
                        <!-- Heroicon name: outline/check -->
                        <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                        </svg>
                      </div>
                      <p class="ml-3 text-base font-medium text-gray-500">
                      Customization
                      </p>
                    </li>

                    <li class="flex items-start">
                      <div class="flex-shrink-0">
                        <!-- Heroicon name: outline/check -->
                        <svg class="flex-shrink-0 h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                        </svg>
                      </div>
                      <p class="ml-3 text-base font-medium text-gray-500">
                      API Integration
                      </p>
                    </li>
                  </ul>
                  <div class="mt-3">
                    <div class="rounded-lg shadow-md">
                      <a href="#" class="block w-full text-center rounded-lg border border-transparent bg-white px-6 py-3 text-base font-medium text-indigo-600 hover:bg-gray-50" aria-describedby="tier-scale">
                        Start your trial
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="bg-white">
  <div class="trial-bg-2">
  <!-- Comparison table -->
  <div class="max-w-2xl mx-auto bg-white py-16 sm:py-16 sm:px-6 lg:max-w-7xl lg:px-8">
    <!-- xs to lg -->
    <div class="space-y-24 lg:hidden">
      <div>
        <div class="px-4">
          <h2 class="text-lg leading-6 font-medium text-gray-900">Basic</h2>
          <p class="mt-4">
            <span class="text-4xl font-extrabold text-gray-900">Rs.0</span>
            <span class="text-base font-medium text-gray-500">/month</span>
          </p>
          <p class="mt-4 text-sm text-gray-500">Basic</p>
          <a href="#" class="background-gradient block w-full border border-transparent  shadow py-2 text-sm font-semibold text-white text-center hover:to-pink-600">Buy Basic</a>
        </div>

       
         <!-- basic table start -->
         <table class="mt-8 w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Pricing
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Billed Quarterly</th>
            <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

            <tr class="border-t border-gray-200">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Billed Monthly</th>
              <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

    </tbody>
</table>

<table class=" w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Portals
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Admin Portal</th>
            <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

            <tr class="border-t border-gray-200">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Manager Portal</th>
              <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

    </tbody>
</table>
<!-- basic table end -->


        <div class="border-t border-gray-200 px-4 pt-5">
          <a href="#" class="background-gradient block w-full border border-transparent  shadow py-2 text-sm font-semibold text-white text-center hover:to-pink-600">Buy Basic</a>
        </div>
      </div>

      <div>
        <div class="px-4">
          <h2 class="text-lg leading-6 font-medium text-gray-900">Essential</h2>
          <p class="mt-4">
            <span class="text-4xl font-extrabold text-gray-900">Rs.149</span>
            <span class="text-base font-medium text-gray-500">/user/month</span>
          </p>
          <p class="mt-4 text-sm text-gray-500">Quis eleifend a tincidunt pellentesque. A tempor in sed.</p>
          <a href="#" class="background-gradient mt-6 block w-full bg-gradient-to-r from-orange-500 to-pink-500 border border-transparent  shadow py-2 text-sm font-semibold text-white text-center hover:to-pink-600">Buy Essential</a>
        </div>
<!-- essential table start -->
<table class="mt-8 w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Pricing
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Billed Quarterly</th>
            <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

            <tr class="border-t border-gray-200">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Billed Monthly</th>
              <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

    </tbody>
</table>

<table class=" w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Portals
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Admin Portal</th>
            <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

            <tr class="border-t border-gray-200">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Manager Portal</th>
              <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Counselor Portal</th>
            <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

    </tbody>
</table>

<table class="w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Lead Management
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Lead Tracking</th>
            <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

            <tr class="border-t border-gray-200">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Lead Qualification</th>
              <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

            <tr class="border-t border-gray-200">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Lead Scoring</th>
              <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

            <tr class="border-t border-gray-200">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Custom Fields</th>
              <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

            <tr class="border-t border-gray-200">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Duplicate Blocking</th>
              <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

    </tbody>
</table>

<table class="w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Advance Lead Management
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Lead Distribution</th>
            <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>


    </tbody>
</table>
<!-- essential table end -->


        <div class="border-t border-gray-200 px-4 pt-5">
          <a href="#" class="background-gradient block w-full bg-gradient-to-r from-orange-500 to-pink-500 border border-transparent  shadow py-2 text-sm font-semibold text-white text-center hover:to-pink-600">Buy Essential</a>
        </div>
      </div>

      <div>
        <div class="px-4">
          <h2 class="text-lg leading-6 font-medium text-gray-900">Premium</h2>
          <p class="mt-4">
            <span class="text-4xl font-extrabold text-gray-900">Rs.349</span>
            <span class="text-base font-medium text-gray-500">/month</span>
          </p>
          <p class="mt-4 text-sm text-gray-500">Orci volutpat ut sed sed neque, dui eget. Quis tristique non.</p>
          <a href="#" class="background-gradient mt-6 block w-full bg-gradient-to-r from-orange-500 to-pink-500 border border-transparent  shadow py-2 text-sm font-semibold text-white text-center hover:to-pink-600">Buy Premium</a>
        </div>
<!-- premium table start -->
<table class="mt-8 w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Pricing
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Billed Quarterly</th>
            <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

            <tr class="border-t border-gray-200">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Billed Monthly</th>
              <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

    </tbody>
</table>

<table class=" w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Portals
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Admin Portal</th>
            <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

            <tr class="border-t border-gray-200">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Manager Portal</th>
              <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Counselor Portal</th>
            <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

    </tbody>
</table>

<table class="w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Lead Management
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Lead Tracking</th>
            <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

            <tr class="border-t border-gray-200">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Lead Qualification</th>
              <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

            <tr class="border-t border-gray-200">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Lead Scoring</th>
              <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

            <tr class="border-t border-gray-200">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Custom Fields</th>
              <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

            <tr class="border-t border-gray-200">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Duplicate Blocking</th>
              <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

    </tbody>
</table>

<table class="w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Advance Lead Management
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Lead Distribution</th>
               <td class="py-3 pr-4">
                  <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                  </svg>
                  <span class="sr-only">Yes</span>
               </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Custom Tabs</th>
               <td class="py-3 pr-4">
                  <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                  </svg>
                  <span class="sr-only">Yes</span>
               </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Custom Actions</th>
               <td class="py-3 pr-4">
                  <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                  </svg>
                  <span class="sr-only">Yes</span>
               </td>
            </tr>

    </tbody>
</table>

<table class="w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Sales Automation
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Quick Leads</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Contacts</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Multiple Account</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Emails</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Advanced Filters</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Reminder</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">SMS Facilities</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Text Message</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>
    </tbody>
</table>

<table class="mt-8 w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Reports
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Standard Reports</th>
            <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

            <tr class="border-t border-gray-200">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Multiple account</th>
              <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

    </tbody>
</table>
<table class="w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Analytics & Measurement 
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Graphical Representation</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Filters</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">KPI</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Dashboard Analytics</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Workflow Reports</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Cohorts</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Webform Analytics</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>   
    </tbody>
</table>
<table class="w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Social
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Data with different social pages</th>
            <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

            <tr class="border-t border-gray-200">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Automatic lead generation with social media</th>
              <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>

    </tbody>
</table>
<table class="w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Marketing Automation
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Email Template</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Mass Emailing</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Campaigns</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Lead Segmentation</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">SMS Templates</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>   
    </tbody>
</table>
<table class="w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Advanced Marketing Automation
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Drip Marketing</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">SMS Gateways</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Dialer</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Email Authentication</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>               
    </tbody>
</table>

<table class="w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Team Formation
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Manager Team</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Calendars</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Lead History</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Follow Ups Report</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Status Updates</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Lead Distribution</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Reporting Tools</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Account Tracking</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>               
    </tbody>
</table>
<table class="w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Data Protection and Security
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Profile</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Organization Hierarchy</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Role-based access</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Refer lead</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>           
    </tbody>
</table>
<table class="w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Customization
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Custom Dashboard</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Custom Button</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Custom Fields</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>

            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Custom Modules</th>
                <td class="py-3 pr-4">
                    
                    <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                    <span class="sr-only">Yes</span>
                </td>
            </tr>           
    </tbody>
</table>
<table class="w-full">
    <caption class="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
    Support
    </caption>
<thead>
            <tr>
              <th class="sr-only" scope="col">Feature</th>
              <th class="sr-only" scope="col">Included</th>
            </tr>
</thead>
         
</table>
<table class=" w-full">
    <tbody>
            <tr class=" border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
              <th class="py-3 px-4 text-sm font-normal text-gray-500 text-left" scope="row">Technical Support</th>
            <td class="py-3 pr-4">
                
                <svg class="ml-auto h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
                <span class="sr-only">Yes</span>
            </td>
            </tr>
    </tbody>
</table>

<!-- premium table end -->


        <div class="border-t border-gray-200 px-4 pt-5">
          <a href="#" class="background-gradient block w-full bg-gradient-to-r from-orange-500 to-pink-500 border border-transparent  shadow py-2 text-sm font-semibold text-white text-center hover:to-pink-600">Buy Premium</a>
        </div>
      </div>
    </div>

    <!-- lg+ -->
    <div class="hidden lg:block">
      <table class="w-full h-px table-fixed">
        <caption class="sr-only">
          Pricing plan comparison
        </caption>
        <thead>
          <tr>
            <th class="pb-4 pl-6 pr-6 text-sm font-medium text-gray-900 text-left" scope="col">
              <span class="sr-only">Feature by</span>
              <span>Plans</span>
            </th>

            <th class="w-1/4 pb-4 px-6 text-lg leading-6 font-medium text-gray-900 text-left" scope="col">Basic</th>

            <th class="w-1/4 pb-4 px-6 text-lg leading-6 font-medium text-gray-900 text-left" scope="col">Essential</th>

            <th class="w-1/4 pb-4 px-6 text-lg leading-6 font-medium text-gray-900 text-left" scope="col">Premium</th>
          </tr>
        </thead>
        <tbody class="border-t border-gray-200 divide-y divide-gray-200">
          <tr>
            <th class="py-8 pl-6 pr-6 align-top text-sm font-medium text-gray-900 text-left" scope="row">Pricing</th>

            <td class="h-full py-8 px-6 align-top">
              <div class="h-full flex flex-col justify-between">
                <div>
                  <p>
                    <span class="text-4xl font-extrabold text-gray-900">Rs.0</span>
                    <span class="text-base font-medium text-gray-500">/month</span>
                  </p>
                   <p class="mt-4 text-sm text-gray-500">Basic</p>
                 <!--  <p class="mt-4 text-sm text-gray-500">Quis suspendisse ut fermentum neque vivamus non tellus.</p> -->
                </div>
                <a href="#" class="background-gradient mt-6 block w-full bg-gradient-to-r from-orange-500 to-pink-500 border border-transparent  shadow py-2 text-sm font-semibold text-white text-center hover:to-pink-600">Buy Basic</a>
              </div>
            </td>

            <td class="h-full py-8 px-6 align-top">
              <div class="h-full flex flex-col justify-between">
                <div>
                  <p>
                    <span class="text-4xl font-extrabold text-gray-900">Rs.149</span>
                    <span class="text-base font-medium text-gray-500">/user/month</span>
                  </p>
                  <p class="mt-4 text-sm text-gray-500">Quis eleifend a tincidunt pellentesque. A tempor in sed.</p>
                </div>
                <a href="#" class="background-gradient mt-6 block w-full bg-gradient-to-r from-orange-500 to-pink-500 border border-transparent  shadow py-2 text-sm font-semibold text-white text-center hover:to-pink-600">Buy Essential</a>
              </div>
            </td>

            <td class="h-full py-8 px-6 align-top">
              <div class="h-full flex flex-col justify-between">
                <div>
                  <p>
                    <span class="text-4xl font-extrabold text-gray-900">Rs.349</span>
                    <span class="text-base font-medium text-gray-500">/month</span>
                  </p>
                  <p class="mt-4 text-sm text-gray-500">Orci volutpat ut sed sed neque, dui eget. Quis tristique non.</p>
                </div>
                <a href="#" class="background-gradient mt-6 block w-full bg-gradient-to-r from-orange-500 to-pink-500 border border-transparent  shadow py-2 text-sm font-semibold text-white text-center hover:to-pink-600">Buy Premium</a>
              </div>
            </td>
          </tr>
          <tr>
            <th class="py-3 pl-6 bg-gray-50 text-sm font-medium text-gray-900 text-left" colspan="4" scope="colgroup">Pricing </th>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Billed Quarterly</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Billed Monthly</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

         

          <!-- <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Convallis.</th>
            <td class="py-3 px-6">
              Heroicon name: solid/minus
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <span class="block text-sm text-gray-700">Up to 20 users</span>
            </td>
            <td class="py-3 px-6">
              <span class="block text-sm text-gray-700">Up to 50 users</span>
            </td>
          </tr> -->

          <tr>
            <th class="py-3 pl-6 bg-gray-50 text-sm font-medium text-gray-900 text-left" colspan="4" scope="colgroup">
              Portals</th>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Admin Portal</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Manager Portal</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Counselor Portal</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 bg-gray-50 text-sm font-medium text-gray-900 text-left" colspan="4" scope="colgroup">
            Lead Management </th>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Lead Tracking</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <!-- <tr>
            <th class="py-3 pl-6 bg-gray-50 text-sm font-medium text-gray-900 text-left" colspan="4" scope="colgroup">Support</th>
          </tr> -->

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Lead Qualification</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Lead Scoring</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Custom Fields</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Duplicate Blocking</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 bg-gray-50 text-sm font-medium text-gray-900 text-left" colspan="4" scope="colgroup">
           Advance Lead Management </th>
          </tr>

          <tr>
          
          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Lead Distribution</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>
          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Custom Tabs</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Custom Actions</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>
          <tr>
            <th class="py-3 pl-6 bg-gray-50 text-sm font-medium text-gray-900 text-left" colspan="4" scope="colgroup">
            Sales Automation </th>
</tr>
          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Quick Leads</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>
          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Contacts </th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>
          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Multiple account</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Emails</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Advanced Filters</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Reminder</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">SMS Facilities </th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Text message</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 bg-gray-50 text-sm font-medium text-gray-900 text-left" colspan="4" scope="colgroup">
            Reports </th>
</tr>
          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Standard Reports</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Multiple account</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>
          

          <tr>
            <th class="py-3 pl-6 bg-gray-50 text-sm font-medium text-gray-900 text-left" colspan="4" scope="colgroup">
            Analytics & Measurement </th>
</tr>
          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Graphical Representation</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Filters</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">KPI</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Dashboard Analytics</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Workflow Reports</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Cohorts</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Webform Analytics</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>
          

          <tr>
            <th class="py-3 pl-6 bg-gray-50 text-sm font-medium text-gray-900 text-left" colspan="4" scope="colgroup">
            Social </th>
</tr>
          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Data with different social pages</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Automatic lead generation with social media</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          
          <tr>
            <th class="py-3 pl-6 bg-gray-50 text-sm font-medium text-gray-900 text-left" colspan="4" scope="colgroup">
            Marketing Automation </th>
</tr>
          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Email Template</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Mass Emailing</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Campaigns</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Lead Segmentation</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">SMS Templates</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>
          

          <tr>
            <th class="py-3 pl-6 bg-gray-50 text-sm font-medium text-gray-900 text-left" colspan="4" scope="colgroup">
            Advanced Marketing Automation </th>
</tr>
          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Drip Marketing</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">SMS Gateways</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Dialer</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Email Authentication</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

         
          <tr>
            <th class="py-3 pl-6 bg-gray-50 text-sm font-medium text-gray-900 text-left" colspan="4" scope="colgroup">
            Team Formation</th>
</tr>
          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Manager Team</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Calendars</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Lead History</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Follow Ups Report</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Status Updates</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>
          
          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Lead Distribution</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Reporting Tools</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Account Tracking</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 bg-gray-50 text-sm font-medium text-gray-900 text-left" colspan="4" scope="colgroup">
            Data Protection and Security
        </th>
</tr>
          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Profile</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Organization Hierarchy</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Role-based access</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Refer lead</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

         
          <tr>
            <th class="py-3 pl-6 bg-gray-50 text-sm font-medium text-gray-900 text-left" colspan="4" scope="colgroup">
            Customization
        </th>
</tr>
          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Custom Dashboard</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Custom Button</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Custom Fields</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Custom Modules</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

         
          <tr>
            <th class="py-3 pl-6 bg-gray-50 text-sm font-medium text-gray-900 text-left" colspan="4" scope="colgroup">
            Support
        </th>
</tr>
          <tr>
            <th class="py-3 pl-6 pr-6 text-sm font-normal text-gray-500 text-left" scope="row">Technical Support</th>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Basic</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/minus -->
              <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Not included in Essential</span>
            </td>
            <td class="py-3 px-6">
              <!-- Heroicon name: solid/check -->
              <svg class="h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
              <span class="sr-only">Included in Premium</span>
            </td>
          </tr>

          
         
          
        </tbody>
        <tfoot>
          <tr class="border-t border-gray-200">
            <th class="sr-only" scope="row">Choose your plan</th>

            <td class="pt-5 px-6">
              <a href="#" class="background-gradient block w-full bg-gradient-to-r from-orange-500 to-pink-500 border border-transparent  shadow py-2 text-sm font-semibold text-white text-center hover:to-pink-600">Buy Basic</a>
            </td>

            <td class="pt-5 px-6">
              <a href="#" class="background-gradient block w-full bg-gradient-to-r from-orange-500 to-pink-500 border border-transparent  shadow py-2 text-sm font-semibold text-white text-center hover:to-pink-600">Buy Essential</a>
            </td>

            <td class="pt-5 px-6">
              <a href="#" class="background-gradient block w-full bg-gradient-to-r from-orange-500 to-pink-500 border border-transparent  shadow py-2 text-sm font-semibold text-white text-center hover:to-pink-600">Buy Premium</a>
            </td>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>

  <!-- Logo cloud -->
  <div class="max-w-7xl mx-auto border-t border-gray-200 py-12 px-4 sm:px-6 lg:py-20 lg:px-8">
    <div class="grid grid-cols-2 gap-8 md:grid-cols-6 lg:grid-cols-5">
      <div class="col-span-1 flex justify-center md:col-span-2 lg:col-span-1">
        <img class="h-12" src="https://tailwindui.com/img/logos/tuple-logo-gray-400.svg" alt="Tuple">
      </div>
      <div class="col-span-1 flex justify-center md:col-span-2 lg:col-span-1">
        <img class="h-12" src="https://tailwindui.com/img/logos/mirage-logo-gray-400.svg" alt="Mirage">
      </div>
      <div class="col-span-1 flex justify-center md:col-span-2 lg:col-span-1">
        <img class="h-12" src="https://tailwindui.com/img/logos/statickit-logo-gray-400.svg" alt="StaticKit">
      </div>
      <div class="col-span-1 flex justify-center md:col-span-3 lg:col-span-1">
        <img class="h-12" src="https://tailwindui.com/img/logos/transistor-logo-gray-400.svg" alt="Transistor">
      </div>
      <div class="col-span-2 flex justify-center md:col-span-3 lg:col-span-1">
        <img class="h-12" src="https://tailwindui.com/img/logos/workcation-logo-gray-400.svg" alt="Workcation">
      </div>
    </div>
  </div>

  <div class="bg-gray-50">
    <!-- FAQ -->
    <div class="max-w-7xl mx-auto py-16 px-4 sm:py-24 sm:px-6 lg:px-8">
      <h2 class="text-3xl font-extrabold text-gray-900 text-center">
        Frequently asked questions
      </h2>
      <div class="mt-12">
        <dl class="space-y-10 md:space-y-0 md:grid md:grid-cols-2 md:grid-rows-2 md:gap-x-8 md:gap-y-12 lg:grid-cols-3">
          <div class="space-y-2">
            <dt class="text-lg leading-6 font-medium text-gray-900">
            How do I get Free Trail of Edutratech CRM? 
            </dt>
            <dd class="text-base text-gray-500">
            To get a free trial of Edutratech CRM, you have to create an account on the Edutratech website. As soon as you create your account you can access the basic free trial pack of Edutratech CRM. 
            </dd>
          </div>
          <div class="space-y-2">
            <dt class="text-lg leading-6 font-medium text-gray-900">
            What do I get in the Trail Pack of Edutratech CRM?
            </dt>
            <dd class="text-base text-gray-500">
            Once you make an account on Eduratech, you can access all the basic features which involve Lead Management and some basic Drip Marketing features. 
            </dd>
          </div>
          <div class="space-y-2">
            <dt class="text-lg leading-6 font-medium text-gray-900">
            How does User Licensing Works?
            </dt>
            <dd class="text-base text-gray-500">
            Edutratech Basic plan supports up to 250 users in any account. However, when a 251 th user is added, you need to purchase 251 user paid license i.e., your entire institution has to move to a paid plan, to continue using our services. You need to complete your account setup to upgrade to a paid plan!
            </dd>
          </div>
          <div class="space-y-2">
            <dt class="text-lg leading-6 font-medium text-gray-900">
            Can I have a customized plan as per my requirement? 
            </dt>
            <dd class="text-base text-gray-500">
            Yes, you can customize your plan according to your need if you are a 500+ institution. 
            </dd>
          </div>
          <div class="space-y-2">
            <dt class="text-lg leading-6 font-medium text-gray-900">
            Where will my data be stored on the cloud? 
            </dt>
            <dd class="text-base text-gray-500">
              Your data will be kept securely on the cloud-hosted by AWS, the name which all the leading companies trust. So, you no need to worry. 
            </dd>
          </div>
          <div class="space-y-2">
            <dt class="text-lg leading-6 font-medium text-gray-900">
            Who will upload the information database over the portal in the initial stage?
            </dt>
            <dd class="text-base text-gray-500">
            The data will be uploaded by you through our bulk uploading option. However, if you need technical assistance in uploading the data, it shall be provided by our team.
            </dd>
          </div>
          <div class="space-y-2">
            <dt class="text-lg leading-6 font-medium text-gray-900">
            Can you help me get started? Do you offer technical consulting?
            </dt>
            <dd class="text-base text-gray-500">
            Yes, we would be delighted. Our Onboarding Team will walk you through the entire initial set-up process to ensure that your institution environment is built for automation. 
            </dd>
          </div>
          <div class="space-y-2">
            <dt class="text-lg leading-6 font-medium text-gray-900">
            If I chose Edutratech, who will be the owner of the data that's on the panel?
            </dt>
            <dd class="text-base text-gray-500">
            It's definitely you, as the data belongs to you and it's your property.
            </dd>
          </div>
          <div class="space-y-2">
            <dt class="text-lg leading-6 font-medium text-gray-900">
            What modes of payment are acceptable?
            </dt>
            <dd class="text-base text-gray-500">
            We accept payment via Visa, MasterCard, and RazorPay. We also accept payment via NEFT bank transfers for yearly subscriptions. For more details, please contact support@edgetechnosoft.com.
            </dd>
          </div>
          <div class="space-y-2">
            <dt class="text-lg leading-6 font-medium text-gray-900">
            Is there an option to upgrade or downgrade my subscription or plan?
            </dt>
            <dd class="text-base text-gray-500">
            Yes, you can upgrade or downgrade your subscription plan at any time as per your need.
            </dd>
          </div>
        </dl>
      </div>
    </div>
  </div>

  <!-- Feature section with brand panel -->
  

  <!-- Footer -->
  
</div>

<?php include 'footer.php' ?>