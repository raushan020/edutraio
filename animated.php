
<div class="integration-con">
    <div class="integration-con-inner">
        <div class="integ-content-con">
            <h2 class="" style="color: #fff;">Tightly integrated with the business apps you use every day</h2><a
                href="login/signup.php"
                class="btn login-btn">START TRAIL<span class="sprite-icns-svg"></span></a>
        </div>
        <div class="integ-img-con">
            <picture class="">
                    <img src="img/mainimg/grouplogo.png" alt="" class="integ-images anim-init socl-img">
            </picture>
            <!-- <picture class="">
                <img
                    src="//www.zohowebstatic.com/sites/default/files/crm/home-integration-illustration.png"
                    class="integration-illustration anim-init" alt="CRM Software">
            </picture>
 -->        </div>
    </div>
</div>