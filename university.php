
<section id="brandSection" class="gradient-bg mt-5">
    <div class="container" style="position: relative; ">
        <div class="row align-items-center">
            <div class="col-md-6 aos-init " data-aos="fade-down" data-aos-duration="800">
                <h1 class="heading">Trusted By <br><span>10+ </span>Universities <br>Globally</h1>
            </div>
            <div class="col-md-6">
                <div class="brands aos-init aos-animate" data-aos="fade-down" data-aos-duration="3000">
                    <div class="row align-items-center">
                        <div class="col-md-4 col-6"><img src="https://ecampus-assets.s3.ap-south-1.amazonaws.com/images/partners/ju-logo.jpg" class="big" alt=""></div>
                        <div class="col-md-4 col-6"><img src="https://ecampus-assets.s3.ap-south-1.amazonaws.com/images/partners/jnu-logo-final_200x53.jpg" alt=""></div>
                        <div class="col-md-4 col-6"><img src="https://ecampus-assets.s3.ap-south-1.amazonaws.com/images/partners/mangalayatan.png" alt=""></div>
                        <div class="col-md-4 col-6"><img src="https://ecampus-assets.s3.ap-south-1.amazonaws.com/images/partners/lingyas_hover.png" class="" alt=""></div>
                        <div class="col-md-4 col-6 mobile-hide"><img src="https://ecampus-assets.s3.ap-south-1.amazonaws.com/images/partners/subharti.png" alt=""></div>
                        <div class="col-md-4 col-6 mobile-hide"><img src="https://ecampus-assets.s3.ap-south-1.amazonaws.com/images/partners/itm_logo.jpg" alt=""></div>
                        <div class="col-md-4 col-6 mobile-hide"><img src="https://ecampus-assets.s3.ap-south-1.amazonaws.com/images/partners/shobit.png" class="big"
                                alt=""></div>
                        <div class="col-md-4 col-6 mobile-hide"><img src="https://ecampus-assets.s3.ap-south-1.amazonaws.com/images/partners/des-logo.jpg" alt=""></div>
                        
                    </div>
                    
                </div>
                <!-- <div class="text-center mt-4"><a href="/universities" > <a class="btn btn-success lg " style="color:white!important">View all Universities</a></a></div> -->
            </div>
        </div>
    </div>
</section>