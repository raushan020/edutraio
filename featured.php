<!-- Featutrd App -->
<link rel="stylesheet" href="css/style.css">
<section style="background-image: url(img/background-1.jpg);" class="pb-5">

<div class="container">
    <div class="section-heading text-center pt-5">
        <h2 class="d-blue bold fadeInUp " data-wow-delay="0.3s">Featured App</h2>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <div class="row region-promo d-flex justify-content-center align-items-center">
                <div class="col-3 d-flex justify-content-center flex-column align-items-center">
                    <img src="img/mainimg/crm.png" class="apps-image mr-2" />
                    <h5 class="mt-4">CRM</h5>
                </div>
                <div class="col-9">
                    <h5>Complete CRM Platform</h5>
                    <p style="font-size: 80%">
                        End-to-end, fully customizable CRM solution for growing
                        businesses and enterprises.
                    </p>

                    <a href="login/signup.php" class="btn login-btn btn-sm rounded-0">Start Trial</a>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="row" style="min-height: 341.984px !important;">
                <div class="col-6 main-apps1 d-flex">
                    <img src="img/mainimg/education.png" class="apps-image mr-2" />
                    <div class="">
                        <h6>Education</h6>
                        <p class="apps-p">Secure business email.</p>
                        <button type="button" class="btn btn-outline-primary btn-sm">
                            Primary
                        </button>
                    </div>
                </div>

                <div class="col-6 main-apps2 d-flex">
                    <img src="img/mainimg/finance.png" class="apps-image mr-2" />
                    <div class="">
                        <h6>Finance</h6>
                        <p class="apps-p">Secure business email.</p>
                        <button type="button" class="btn btn-outline-primary btn-sm">
                            Primary
                        </button>
                    </div>
                </div>


                <div class="col-6 main-apps3 d-flex">
                    <img src="img/mainimg/eccomerce.png" class="apps-image mr-2" />
                    <div class="">
                        <h6>E-commerce</h6>
                        <p class="apps-p">Secure business email.</p>
                        <button type="button" class="btn btn-outline-primary btn-sm">
                            Primary
                        </button>
                    </div>
                </div>


                <div class="col-6 main-apps4 d-flex">
                    <img src="img/mainimg/other.png" class="apps-image mr-2" />
                    <div class="">
                        <h6>Others</h6>
                        <p class="apps-p">Secure business email.</p>
                        <button type="button" class="btn btn-outline-primary btn-sm">
                            Primary
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>
</section>
