<?php include ('header.php')?>
<link src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/css/swiper.css">
<style type="">
    @keyframes changeLetter {
  0% {
    content: "Admission ";
  }

 
25%{
    content: "Marketing Automation"
}
50%{
    content: "Lead Nurturing"
}
 

  100% {
    content: "Lead Management"
  }

 
}
 @keyframes changecss {
    0% {
     border-color: #086ad8;
  }
   50% {
     border-color: transparent;
  }
   100% {
     border-color:#086ad8;
  }

 }

.element{
    border-bottom: 3px solid;
    animation: changecss 3s linear infinite alternate;
    color:#086ad8;
    font-family: Colfax,Helvetica,Arial,sans-serif;
    font-size: 48px;
}

.element:before{
     transition-delay: 3s;
    animation: changeBorder 3s linear infinite alternate;
    display: inline-block;
    content: "";
}

.element:after {
     transition-delay: 5s;
    animation: changeLetter 5s linear infinite alternate;
    display: inline-block;
    content: "";
    
    margin-left: 8px;
    /*border-bottom: 5px solid red;*/
}

.dream-btn-4{
    font-family: Colfax-Bold,Helvetica,Arial,sans-serif;
    position: relative;
    z-index: 1;
    min-width: 160px;
    height: 48px;
    line-height: 35px !important;
    font-size: 16px !important;
    font-weight: 600  !important;
    letter-spacing: 1px;
    display: inline-block;
    padding: 0 20px i !important;
    text-align: center !important;
    text-transform: uppercase;
    background-size: 200% auto;
    color:#fff;
    transition: all 500ms;
    border-radius: 0px  !important;
    border: 1px solid #fff !important;
}
.fadeInUp a:hover  {
  background-color: #086AD8;
  color: #fff ;
}
.dream-btn-5{
    background: linear-gradient(
150deg
, #ff3f85 0, #faa720 100%) !important;
font-family: Colfax-Bold,Helvetica,Arial,sans-serif;
    position: relative;
    z-index: 1;
    min-width: 160px;
    height: 48px;
    line-height: 35px !important;
    font-size: 16px !important;
    font-weight: 600  !important;
    letter-spacing: 1px;
    display: inline-block;
    padding: 0 20px i !important;
    text-align: center !important;
    text-transform: uppercase;
    background-size: 200% auto;
    color:#fff;
   
    transition: all 500ms;
    border-radius: 0px  !important;
    
}
.swiper-container {
  width: 80vw;
}

.swiper-slide {
  background-size: cover;
  background-position: 50%;
  min-height: 80vh;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
}

.swiper-pagination-bullet {
  background-color: transparent;
  border: 2px solid #fff;
  border-radius: 50%;
  width: 12px;
  height: 12px;
  opacity: 1;
}
.swiper-pagination-bullet-active {
  background-color: #fff;
}

.swiper-button-container {
  background-color: rgba(0, 0, 0, 0.25);
}
.swiper-button-prev {
  background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23ffffff'%2F%3E%3C%2Fsvg%3E");
}
.swiper-button-next {
  background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23ffffff'%2F%3E%3C%2Fsvg%3E");
}



.swiper-slide:before {
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: black;
  opacity: 0.4;
}
.swiper-slide h2 {
  pointer-events: none;
  opacity: 0;
  color: #ffffff;
  font-size: calc(5vw);
  letter-spacing: -1px;
  transform: translateY(-5%) scale(0.8);
  transition: 1s ease;
  text-transform: uppercase;
  text-shadow: 0 5px 5px rgba(0, 0, 0, 0.01);
}
.swiper-slide-active h2 {
  opacity: 1;
  transform: translateY(0%) scale(1);
  transition: 1s ease;
}
.headerbtn {
    color: #fff!important;
  }
  .sticky .headerbtn{
  color: #000 !important
}
.btn-clr {
    border: 1px solid #fff !important;
}

@font-face {
  font-family: "San Francisco Display Semibold";
  font-style: normal;
  font-weight: 400;
  src: url(https://applesocial.s3.amazonaws.com/assets/styles/fonts/sanfrancisco/sanfranciscodisplay-semibold-webfont.eot?#iefix) format("embedded-opentype"), url(https://applesocial.s3.amazonaws.com/assets/styles/fonts/sanfrancisco/sanfranciscodisplay-semibold-webfont.woff2) format("woff2"), url(https://applesocial.s3.amazonaws.com/assets/styles/fonts/sanfrancisco/sanfranciscodisplay-semibold-webfont.woff) format("woff"), url(https://applesocial.s3.amazonaws.com/assets/styles/fonts/sanfrancisco/sanfranciscodisplay-semibold-webfont.ttf) format("truetype"), url("fonts/sanfrancisco/sanfranciscodisplay-semibold-webfont.svg#San Francisco Display Semibold") format("svg");
}
.zoom:hover {
    transform: scale(0.1); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
    transition:all .5s;
  }

      .zoom{
   
  transition:all .5s;
 
 
    }
    .header-logo{
  filter: invert(1);
}
.sticky .header-logo{
  filter:  invert(0);
}
</style>
<section class="hero-section ai5 relative section-over" style="min-height: 100vh !important;" id="home">
        <div class="overlay"></div>
        <!-- Hero Content -->
        <div class="hero-section-content">

            <div class="container ">
                <div class="row align-items-center justify-content-center">
                    <!-- Welcome Content -->
                    <div class="col-12 col-lg-12 col-md-12">
                        <div class="welcome-content text-center">

                            <h3 class="bold w-text wow fadeInUp mobile-h3" style="font-family: Colfax,Helvetica,Arial,sans-serif;
    font-size: 44px;font-weight:500 !important;" data-wow-delay="0.2s">Engine of Growth for<strong class="element"></strong></h3>
                           <!--  <div class="line"></div> -->
                            <p class="wow fadeInUp text-20 p-black" data-wow-delay="0.3s" >Partner with the most advance CRM platform to get higher conversion of your leads. <br>Monitor, track and scale to convert from prospect to sales, <span>FASTER.</span></p>
                            <div class="dream-btn-group fadeInUp" data-wow-delay="0.4s">
                                <a href="login/signup.php" class="btn dream-btn-4 ">get started</a>
                                <a href="login/signup.php" class="btn dream-btn-5"> Request demo</a>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-12 col-lg-6 col-md-12">
                        <img src="img/mainimg/mainbanner-2.png" class="mt-s" alt="">
                    </div> -->
                </div>
            </div>
        </div>
    </section>
     <?php include ('slider.php')?>
<!--     <div class="parttns zoom">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                   
                    <img src="img/Chandigarh-University-color.png" class="uni-img" alt="">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                    <img src="img/cv-color.png" alt="" class="uni-img">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                    <img src="img/itm_logo_hover-removebg-preview.png" alt="" class="uni-img">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                    <img src="img/Jaipur-National-University-color.png" alt="" class="uni-img">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                    <img src="img/Lingayas-Vidyapeeth-color.png" alt="" class="uni-img">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                    <img src="img/Shri-venkateshwar-University-color.png" alt="" class="uni-img">
                </div>
            </div>
        </div>
    </div>
 -->
   <!--  3rd section -->
    <!-- This example requires Tailwind CSS v2.0+ -->
<div class="relative bg-white py-8 sm:py-12 lg:py-10">
  <div class="mx-auto max-w-md px-4 text-center sm:max-w-3xl sm:px-6 lg:px-8 lg:max-w-7xl">
    <h2 class="text-base font-semibold tracking-wider text-indigo-600 uppercase">Deploy faster</h2>
    <p class="mt-2 text-3xl font-extrabold text-gray-900 tracking-tight sm:text-4xl">
      Everything you need to deploy your app
    </p>
    <p class="max-w-prose mx-auto text-xl text-gray-500">
      Phasellus lorem quam molestie id quisque diam aenean nulla in. Accumsan in quis quis nunc, ullamcorper malesuada. Eleifend condimentum id viverra nulla.
    </p>
    <div class="mt-12">
      <div class="grid grid-cols-1 gap-8 sm:grid-cols-2 lg:grid-cols-3">
        <div class="pt-6">
          <div class="flow-root bg-gray-50 rounded-lg px-6 pb-8">
            <div class="-mt-6">
              <div>
                <span class="inline-flex items-center justify-center p-3 bg-indigo-500 rounded-md shadow-lg">
                  <!-- Heroicon name: outline/cloud-upload -->
                  <svg class="h-6 w-6 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12" />
                  </svg>
                </span>
              </div>
              <h3 class="mt-8 text-lg font-medium text-gray-900 tracking-tight">Push to Deploy</h3>
              <p class="mt-5 text-base text-gray-500">
                Ac tincidunt sapien vehicula erat auctor pellentesque rhoncus. Et magna sit morbi lobortis.
              </p>
            </div>
          </div>
        </div>

        <div class="pt-6">
          <div class="flow-root bg-gray-50 rounded-lg px-6 pb-8">
            <div class="-mt-6">
              <div>
                <span class="inline-flex items-center justify-center p-3 bg-indigo-500 rounded-md shadow-lg">
                  <!-- Heroicon name: outline/lock-closed -->
                  <svg class="h-6 w-6 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z" />
                  </svg>
                </span>
              </div>
              <h3 class="mt-8 text-lg font-medium text-gray-900 tracking-tight">SSL Certificates</h3>
              <p class="mt-5 text-base text-gray-500">
                Ac tincidunt sapien vehicula erat auctor pellentesque rhoncus. Et magna sit morbi lobortis.
              </p>
            </div>
          </div>
        </div>

        <div class="pt-6">
          <div class="flow-root bg-gray-50 rounded-lg px-6 pb-8">
            <div class="-mt-6">
              <div>
                <span class="inline-flex items-center justify-center p-3 bg-indigo-500 rounded-md shadow-lg">
                  <!-- Heroicon name: outline/refresh -->
                  <svg class="h-6 w-6 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15" />
                  </svg>
                </span>
              </div>
              <h3 class="mt-8 text-lg font-medium text-gray-900 tracking-tight">Simple Queues</h3>
              <p class="mt-5 text-base text-gray-500">
                Ac tincidunt sapien vehicula erat auctor pellentesque rhoncus. Et magna sit morbi lobortis.
              </p>
            </div>
          </div>
        </div>

        <div class="pt-6">
          <div class="flow-root bg-gray-50 rounded-lg px-6 pb-8">
            <div class="-mt-6">
              <div>
                <span class="inline-flex items-center justify-center p-3 bg-indigo-500 rounded-md shadow-lg">
                  <!-- Heroicon name: outline/shield-check -->
                  <svg class="h-6 w-6 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m5.618-4.016A11.955 11.955 0 0112 2.944a11.955 11.955 0 01-8.618 3.04A12.02 12.02 0 003 9c0 5.591 3.824 10.29 9 11.622 5.176-1.332 9-6.03 9-11.622 0-1.042-.133-2.052-.382-3.016z" />
                  </svg>
                </span>
              </div>
              <h3 class="mt-8 text-lg font-medium text-gray-900 tracking-tight">Advanced Security</h3>
              <p class="mt-5 text-base text-gray-500">
                Ac tincidunt sapien vehicula erat auctor pellentesque rhoncus. Et magna sit morbi lobortis.
              </p>
            </div>
          </div>
        </div>

        <div class="pt-6">
          <div class="flow-root bg-gray-50 rounded-lg px-6 pb-8">
            <div class="-mt-6">
              <div>
                <span class="inline-flex items-center justify-center p-3 bg-indigo-500 rounded-md shadow-lg">
                  <!-- Heroicon name: outline/cog -->
                  <svg class="h-6 w-6 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                  </svg>
                </span>
              </div>
              <h3 class="mt-8 text-lg font-medium text-gray-900 tracking-tight">Powerful API</h3>
              <p class="mt-5 text-base text-gray-500">
                Ac tincidunt sapien vehicula erat auctor pellentesque rhoncus. Et magna sit morbi lobortis.
              </p>
            </div>
          </div>
        </div>

        <div class="pt-6">
          <div class="flow-root bg-gray-50 rounded-lg px-6 pb-8">
            <div class="-mt-6">
              <div>
                <span class="inline-flex items-center justify-center p-3 bg-indigo-500 rounded-md shadow-lg">
                  <!-- Heroicon name: outline/server -->
                  <svg class="h-6 w-6 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h14M5 12a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v4a2 2 0 01-2 2M5 12a2 2 0 00-2 2v4a2 2 0 002 2h14a2 2 0 002-2v-4a2 2 0 00-2-2m-2-4h.01M17 16h.01" />
                  </svg>
                </span>
              </div>
              <h3 class="mt-8 text-lg font-medium text-gray-900 tracking-tight">Database Backups</h3>
              <p class="mt-5 text-base text-gray-500">
                Ac tincidunt sapien vehicula erat auctor pellentesque rhoncus. Et magna sit morbi lobortis.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

    <section class="how section-padding-100-70 relative hex-pat-2 section-over-2" >

        <div class="container">

            <div class="section-heading text-center">
               
                <h2 class="heading text-center text-white" data-wow-delay="0.3s">Edutratech CRM</h2>
                <p class="wow fadeInUp text-white" data-wow-delay="0.4s">Simplification of Business through Magnification of Sales and Efficient Marketing under one
Umbrella of CRM</p>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <!-- Content -->
                    <div class="service_single_content box-shadow text-center mb-100 wow fadeInUp"
                        data-wow-delay="0.2s">
                        <!-- Icon -->
                        <div class="how_icon">
                            <img src="img/mainimg/marketing.jpg" class="colored-icon" alt="">
                        </div>
                        <h6>Marketing zone</h6>
                        <p>It helps in creating good leads through marketing in various social media networking
                            sites such as doing campaigns on Facebook, Instagram, YouTube, and many others.</p>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <!-- Content -->
                    <div class="service_single_content box-shadow text-center mb-100 wow wow fadeInUp"
                        data-wow-delay="0.3s">
                        <!-- Icon -->
                        <div class="how_icon">
                            <img src="img/mainimg/sales.jpg" class="colored-icon" alt="">
                        </div>
                        <h6>Sales zone</h6>
                        <p>After fetching leads from numerous campaigns, sales team starts working on it and tag them with the actual position of the lead such as hot, cold, warm,
                            interested or    not-interested.</p>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <!-- Content -->
                    <div class="service_single_content box-shadow text-center mb-100 wow fadeInUp"
                        data-wow-delay="0.4s">
                        <!-- Icon -->
                        <div class="how_icon">
                            <img src="img/mainimg/customer-realtion.jpg" class="colored-icon" alt="">
                        </div>
                        <h6>Relationship building zone</h6>
                        <p>With the work on leads by our sales team it creates a relationship with the
                            customer by not letting our customers wait and ensuring the instant act over leads to
                            increase sales
                            analytics.</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- ##### Welcome Area End ##### -->
    <!--social media image -->
    <?php include ('socialmedia.php')?>
    <!-- video section -->
    <?php include ('videoSection.php')?>
  <?php include ('UniValley.php')?> 
 
      <!-- <div class="footer-bg" style="background-image: url(http://localhost/edutratech/img/mainimg/footer-bg.png);"> -->
        <!-- ##### Testimonial Area Start ##### -->
        <section class="clients_testimonials_area bg-img section-padding-50-0" id="test" style="background-image: url(img/bg-3.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-heading text-center">
                          
                            <h2 class="heading text-center text-white" data-wow-delay="0.3s">Loved By Our Clients</h2>
                            <p class="wow fadeInUp text-white" data-wow-delay="0.4s">Simplified panel, full customization, excellent service support and what not <br>Listen Directly
from them </p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="testslider" class="container-fluid owl-carousel owl-theme"style="display: block !important;">
                <div class="row justify-content-center wow fadeInUp" data-wow-delay="0.4s">
                    <div class="col-12 col-md-10 col-xs-10 offset-xs-1 pb-4">
                        <div class="client_slides owl-carousel">

                            <!-- Single Testimonial -->
                            <div class="single-testimonial text-center">
                                <!-- Testimonial Image -->
                                <div class="testimonial_image">
                                    <img src="img/test-img/1.jpg" alt="">
                                </div>
                                <!-- Testimonial Feedback Text -->
                                <div class="testimonial-description">
                                    <div class="testimonial_text">
                                        <p>When it comes to Sales Automation, My No. 1 go-to tool is Edutra CRM. It is simplicity mixed with advanced operation. The functionality that I most love about Edutra CRM is the automation and the range of variables it provides to create deep-rooted campaigning on different channels.
                                              </p>
                                    </div>

                                    <!-- Admin Text -->
                                    <div class="admin_text">
                                        <h5>Bharat Aggarwal</h5>
                                        <p>Head of Operations, SV University</p>
                                    </div>
                                </div>
                            </div>

                            <!-- Single Testimonial -->
                            <div class="single-testimonial text-center">
                                <!-- Testimonial Image -->
                                <div class="testimonial_image">
                                    <img src="img/test-img/2.jpg" alt="">
                                </div>
                                <!-- Testimonial Feedback Text  -->
                                <div class="testimonial-description">
                                    <div class="testimonial_text">
                                        <p>Edutra CRM is seriously fast and easy to implement. The service support provided by Eductra is top-notch and exemplary. 
                                            The tool provides you super flexibility according to demand and email automation is most loved by me. I would highly recommend the software. </p>
                                    </div>

                                    <!-- Admin Text -->
                                    <div class="admin_text">
                                        <h5>Udham Sharma</h5>
                                        <p>Program Coordinator, Jaipur National University</p>
                                    </div>
                                </div>
                            </div>

                            <!-- Single Testimonial -->
                            <div class="single-testimonial text-center">
                                <!-- Testimonial Image -->
                                <div class="testimonial_image">
                                    <img src="img/test-img/3.jpg" alt="">
                                </div>
                                <!-- Testimonial Feedback Text  -->
                                <div class="testimonial-description">
                                    <div class="testimonial_text">
                                        <p>
                                             Edura CRM is loaded with tonnes of features that will help you achieve more personalized effects with the leads at different stages. Utilizing the marketing campaign Automation helped me make an everlasting relationship with my clients and Edutra CRM is my only sales tool. </p>
                                    </div>
                                    <!-- Admin Text -->
                                    <div class="admin_text">
                                        <h5>Mohit Singh</h5>
                                        <p>Head of Marketing, ITM College</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/js/swiper.js"></script>
        <script>
            var Swipes = new Swiper('.swiper-container', {
    loop: true,
    slidesPerView: 3,
    spaceBetween: 10,
    
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: '.swiper-pagination',
    },
});
        </script>


        <?php include ('footer.php')?>

        <!-- ##### Testimonial Area End ##### -->
<!-- 
      