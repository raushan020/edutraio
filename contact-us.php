
   <!-- ##### Header Area Start ##### -->
    <?php
    include ('header.php')
    ?>
    <style type="">
        .my-font{
            font-style: normal !important;
    font-family: "Montserrat", sans-serif !important;
        }
         
    </style>
    <!-- ##### Header Area End ##### -->

    <!-- ##### Welcome Area Start ##### -->
    <!-- <div class="breadcumb-area">
        breadcumb content
        <div class="breadcumb-content">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <nav aria-label="breadcrumb" class="breadcumb--con text-center">
                            <h2 class="w-text title wow fadeInUp" data-wow-delay="0.2s">Contact Us</h2>
                            <ol class="breadcrumb justify-content-center wow fadeInUp" data-wow-delay="0.4s">
                                <li class="breadcrumb-item"><a href="contact-us.php#">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- ##### Welcome Area End ##### -->

    <!-- ##### Contact Area Start ##### -->
    <section class="gradient-bg mt-10 ">
    <div class="container-flex">
        <div class="breadcrumb-area">
            
                <h1 class="text-4xl font-extrabold text-black sm:text-5xl sm:tracking-tight lg:text-5xl text-center">
                    <span>Contact-Us</span></h1>
        
        </div>
      </div>
    </section>
    <section class="section-padding-100 contact_us_area" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading text-center">
                       
                        <h2 class="wow fadeInUp" data-wow-delay="0.3s">We’d Love to Connect</h2>
                        <p class="wow fadeInUp" data-wow-delay="0.4s">
                        Over a cup of coffee, let us demonstrate How Edutractech will simply all your business needs and
managing all your business activities under one unified platform.</p>
                    </div>
                </div>
            </div>

            <!-- Contact Form -->
            <div class="row justify-content-center">
                <div class="col-12 col-md-6 col-lg-6">
                   
                <ul>
                <li><h3>For Any Information,</h3></li>
               <li> <p><strong>Please Fill The Form Or Call Us.</strong></p></li>

                <li class="dis-flex"><i class="fa fa-home fa-2x" aria-hidden="true"></i><p class="ml-2">B-136, Sector-2, Noida</p></li>
                <li class="dis-flex"><i class="fa fa-phone fa-2x" aria-hidden="true"></i>
<p class="ml-2">+91 9911331937</p></li>
                <li class="dis-flex fa-2x"><i class="fa fa-envelope" aria-hidden="true"></i>
<p class="ml-2">support@edutratech.com</p></li>
            </ul>
       
                </div>
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="contact_form about-div-1">
                        <form action="contact-us.php#" method="post" id="main_contact_form" novalidate>
                            <div class="row">
                                <div class="col-12">
                                    <div id="success_fail_info"></div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="group wow fadeInUp" data-wow-delay="0.2s">
                                        <input  class="my-font" type="text" name="fname" id="fname" required>
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label  class="my-font">First Name</label>
                                    </div>
                                </div>
                                 <div class="col-12 col-md-6">
                                    <div class="group wow fadeInUp" data-wow-delay="0.2s">
                                        <input  class="my-font" type="text" name="lname" id="lname" required>
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label class="my-font">Last Name</label>
                                    </div>
                                </div>
                               
                                 <div class="col-12 col-md-6">
                                    <div class="group wow fadeInUp" data-wow-delay="0.3s">
                                        <input  class="my-font" type="Number" name="phone" id="phone" required>
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label  class="my-font">Phone Number</label>
                                    </div>
                                </div>
                                 <div class="col-12 col-md-6">
                                    <div class="group wow fadeInUp" data-wow-delay="0.3s">
                                        <input  class="my-font" type="text" name="email" id="email" required>
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label  class="my-font">Email</label>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="group wow fadeInUp" data-wow-delay="0.4s">
                                        <input  class="my-font" type="text" name="institute" id="institute" required>
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label  class="my-font">Institution</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="group wow fadeInUp" data-wow-delay="0.5s">
                                        <textarea  class="my-font" name="message" id="message" ></textarea>
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label  class="my-font">Please Describe Your Requirements</label>
                                    </div>
                                </div>
                                <div class="col-12 text-center wow fadeInUp" data-wow-delay="0.6s">
                                    <button type="submit" class="btn btn-primary dream-btn">Send Message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Contact Area End ##### -->
    

    <!-- ##### Footer Area Start ##### -->
   <?php include 'footer.php' ?>