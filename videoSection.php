<link rel="stylesheet" href="css/style.css" />

<div class="container mb-5 mt-5">
  <div class="section-heading text-center">
    <h2 class="heading text-center" data-wow-delay="0.3s">Drip Marketing</h2>
    <p class="fadeInUp d-blue " data-wow-delay="0.4s">
      Make your own rules and automate marketing campaigns
    </p>
  </div>
  <div class="row pb-5">
    <div class="col-md-6 background-color pt-5">
      <h2>Automate & Schedule Everything</h2>
      <ul>
        <li class="list-item pt-4"><i class="fa fa-check" aria-hidden="true"></i>
        	Customize and upload your marketing collateral seamlessly and chose the automatic workflow.
        </li>
        <li class="list-item pt-4"><i class="fa fa-check" aria-hidden="true"></i>
        	Detailed market targeting and target penetration on every communication platform.
        </li>
       <!--  <li class="list-item pt-4">
          Automate every aspect of your business and cut out time-intensive,
          repetitive tasks
        </li> -->
        <li>
          <a href="" class="learnmore blue-arrow nonlang pt-4"
            >Find out more ways to automate your sales<span
              class="sprite-icns-svg"
            ></span
          ></a>
        </li>
      </ul>
    </div>

    <div class="col-md-6 videoSec ">
      <img src="img/GIF.gif" class="img-fluid">
    </div>
  </div>
</div>
