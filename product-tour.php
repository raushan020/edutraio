<?php include ('header.php')?>
 <section class="gradient-bg mt-10 ">
    <div class="container-flex">
        <div class="breadcrumb-area">
            
                <h1 class="text-4xl font-extrabold text-black sm:text-5xl sm:tracking-tight lg:text-5xl text-center">
                    <span>Product-Tour</span></h1>
        
        </div>
      </div>
    </section>
<section class="gradient-bg  pt-5" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5 class="heading text-center">
                    <span>Connect all the dots with one powerful solution </span> powerful solution</h5>
            </div>
        </div>
        <div class="row align-items-center mt-md-5">
            <div class="col-md-4">
                <!-- <h5 class="">Heatmaps</h5> -->
                <p class=" text-left mb-5"><strong>Admin/Super Admin –</strong>  work is to operate administration. In this dashboard, an admin will get access to handle and manages universities, managers, counselors, and lead management. It all comes under the supervision of the admin.</p>
                <div class="nav flex-column  nav-pills mb-4" id="" role="tablist" aria-orientation="vertical">
                    <a class=" my-button active" onclick="displayImage('https://ecampus-assets.s3.ap-south-1.amazonaws.com/images/product-tour/crm/sUPER+ADMIN'.jpg','')" data-toggle="pill" href=""  role="tab" aria-controls="v-pills-0-0" aria-selected="true">
                        <span> Admin Dashboard </span></a>
                    <a class="my-button " onclick="displayImage('https://ecampus-assets.s3.ap-south-1.amazonaws.com/images/product-tour/crm/all+lead.jpg','140px')" data-toggle="pill" href="" role="tab" aria-controls="v-pills-0-1" aria-selected="false">
                        <span>Student Management</span></a>
                    <a class="my-button " onclick="displayImage('https://ecampus-assets.s3.ap-south-1.amazonaws.com/images/product-tour/crm/University.jpg','280px')" data-toggle="pill" href="" role="tab" aria-controls="v-pills-0-2" aria-selected="false">
                        <span>Courseware </span></a>
                    <a class="my-button  " onclick="displayImage('https://ecampus-assets.s3.ap-south-1.amazonaws.com/images/product-tour/crm/Campaign.jpg','420px')" data-toggle="pill" href="" role="tab" aria-controls="v-pills-0-3" aria-selected="true">
                        <span> Timetable Management</span> </a>
                </div>
            </div>
            <div class="col-md-1" >
                <div class="row" id="arrow-icon">
                <img src="https://static-cms.hotjar.com/original_images/arrow-blue.png">

                </div>
            </div>
            <div class="col-md-7 ">
                <div class="tab-content " id="v-pills-tabContent">
                    <div class="tab-pane fade active show" id="edu-crm">
                        <img src="https://ecampus-assets.s3.ap-south-1.amazonaws.com/images/product-tour/crm/sUPER+ADMIN'.jpg" class="img-fluid shado" alt="Admin Dashboard">
                    </div>
                 
                </div>
            </div>
        </div>
    </div>
</section>
<script>
function displayImage(url,val){
    document.getElementById('edu-crm').innerHTML = '<img src='+url+' class="img-fluid shado" alt="Admin Dashboard">';
    document.getElementById('arrow-icon').style.marginTop = val;
    document.getElementById("edu-crm").style.transition = "opacity .5s ease-out";

}
</script>

<!-- second section -->
<section class="pt-5" style=" background-image: linear-gradient(to right, #0000000f, #fff);">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5 class="heading text-center"> <span>Connect all the dots with one powerful solution </span> powerful solution</h5>
            </div>
        </div>
        <div class="row align-items-center mt-md-5">
          <div class="col-md-7">
              <div class="tab-content mb-4">
                  <div class="tab-pane fade active show" id="edu-crmnew">
                    <img src="https://ecampus-assets.s3.ap-south-1.amazonaws.com/images/product-tour/counsellor/All+leads.jpg"
                    class="img-fluid shado"
                    alt="crm">
                  </div>
              </div>
          </div> 
          <div class="col-md-1">
            <div class="row" id="arrow-icon-1">
              <img src="https://static-cms.hotjar.com/original_images/arrow-blue.png"
              style="margin-top: 95px">
          </div>
          </div>
          <div class="col-md-4">
          <strong><h5>Counsellor</h5></strong>
              <p class="text-left mb-5"><strong></strong> In Counselors' dashboard, consists of managing the student lead and handling its admission, assignments database, semester examination, and resolving all the queries in between the program.</p>
            <div class="nav flex-column" >
               <a class="my-button active " onclick="displayImageOne('https://ecampus-assets.s3.ap-south-1.amazonaws.com/images/product-tour/counsellor/All+leads.jpg','')" aria-selected="true" data-toggle="pill"><span>All leads</span></a>
               <a class="my-button" onclick="displayImageOne('https://ecampus-assets.s3.ap-south-1.amazonaws.com/images/product-tour/counsellor/Cousellor.jpg','105px')" aria-selected="false" data-toggle="pill"><span>Counseller</span></a>
               <a class="my-button" onclick="displayImageOne('https://ecampus-assets.s3.ap-south-1.amazonaws.com/images/product-tour/counsellor/Follow+Up.jpg','210px')" aria-selected="false" data-toggle="pill"><span>Follow Up</span></a>
              
          </div> 
        </div>
    </div>
</section>
<script>
    function displayImageOne(url,val){
        document.getElementById("edu-crmnew").innerHTML='<img src='+url+' class="img-fluid shado" alt="Admin Dashboard">';
        document.getElementById("arrow-icon-1").style.marginTop=val;
    }
</script>
<section class="pt-3" style="background-image: url(img/background-1.jpg);">
    <div class="container">
        <div class="row align-items-center mt-md-5">
            <div class="col-md-6">
                <ul>
                <li><h2>Get accurate forecasts</h2></li>
               <li> <p><strong>Use actionable data to make better decisions.</strong></p></li>

                <li class="dis-flex"><span class="fa-stack fa-lg">
  <i class="fa fa-circle-thin fa-stack-2x"></i>
  <i class="fa fa-search fa-stack-1x"></i>
</span><p class="ml-2">Get the insights you need to make smarter decisions.</p></li>
                <li class="dis-flex"><span class="fa-stack fa-lg">
  <i class="fa fa-circle-thin fa-stack-2x"></i>
  <i class="fa fa-search fa-stack-1x"></i>
</span><p class="ml-2">Get the insights you need to make smarter decisions.</p></li>
                <li class="dis-flex"><span class="fa-stack fa-lg">
  <i class="fa fa-circle-thin fa-stack-2x"></i>
  <i class="fa fa-search fa-stack-1x"></i>
</span><p class="ml-2">Get the insights you need to make smarter decisions.</p></li>
            </ul>
            </div>
            
             <div class="col-md-6 mb-4">
                  <img src="https://ecampus-assets.s3.ap-south-1.amazonaws.com/images/product-tour/counsellor/All+leads.jpg"
                    class="img-fluid shado"
                    alt="crm">
             </div>
        </div>
    </div>
</section>
<?php include ('footer.php')?>