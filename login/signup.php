<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="description" content="" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

  <title>Edutratech | Free Trial</title>

  <!-- Favicon -->
  <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
  <!-- CSS here -->
  <link rel="stylesheet" href="css/style.css" />

  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

  <script src="signup.js"></script>
</head>


<body style="background: linear-gradient(90deg, rgba(8,106,216,1) 0%, rgba(8,106,216,1) 35%, rgba(0,212,255,1) 100%);">
  <!-- PreLoader -->
  <!--  <div id="preloader">-->
  <!--    <div id="status">-->
  <!--      <div class="spinner"></div>-->
  <!--    </div>-->
  <!--  </div>-->
  <!--Preloader-->

  <!-- ======================================
    ******* Page Wrapper Area Start **********
    ======================================= -->
  <div class="main-content-background-img">
    <div class="container h-100">
      <div class="row h-100 align-items-center justify-content-center">
        <div class="col-md-8 col-lg-8">
          <!-- Middle Box -->
          <div class="middle-box mt-60 login-bg">
            <div class="col-12 col-md-12 col-lg-12 col-xl-12 px-lg-12 my-5">
              <div class="login-signup-wrap px-4 px-lg-5 pt-20 my-5">
                <!-- Heading -->
                <h1 class="text-center mb-1">
                  Registration
                </h1>

                <!-- Subheading -->
                <p class="text-muted text-center mb-5">
                  Get access to a world of possibilities.
                </p>

                <p>

                </p>

                <!-- Form -->
                <form class="login-signup-form" id='form_reg' method="post">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <!-- Label -->
                        <label class="pb-1">
                          First Name
                        </label>
                        <!-- Input group -->
                        <div class="input-group input-group-merge">
                          <div class="input-icon">
                            <i class="ri-mail-line color-primary"></i>
                          </div>
                          <input id="fname" type="text" class="form-control" placeholder="Name" required />
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <!-- Label -->
                        <label class="pb-1" id="lstname">
                          Last name
                        </label>
                        <!-- Input group -->
                        <div class="input-group input-group-merge">
                          <div class="input-icon">
                            <i class="ri-mail-line color-primary"></i>
                          </div>
                          <input id="sname" type="text" class="form-control" placeholder="surname" required />
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <!-- Label -->
                        <label class="pb-1">
                          Institution Name
                        </label>
                        <!-- Input group -->
                        <div class="input-group input-group-merge">
                          <div class="input-icon">
                            <i class="ri-mail-line color-primary"></i>
                          </div>
                          <input id="institution" type="text" class="form-control" placeholder=" Institution Name"
                            required />
                        </div>
                      </div>
                    </div>


                    <div class="col-md-6" id="checkbox-form">
                      <div class="form-group">
                        <!-- Label -->
                        <label class="pb-1">
                          Select University
                        </label>
                        <!-- Input group -->
                        <div class="input-group input-group-merge row">
                          <div>
                            <input id="shriVenteshwar" type="checkbox" name="universities"
                              value="Shri Venkateshwar University" onclick="showHideInfo()">
                            <label for="shriVenteshwar">Shri Venkateshwar University</label>
                          </div>

                          <div>
                            <input type="checkbox" id="chandigarh" onclick="showHideInfo()"
                              value="Chandigarh university" name="universities">
                            <label for="chandigarh">Chandigarh university</label>
                          </div>
                        </div>
                      </div>
                    </div>



                    <div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <!-- Label -->
                            <label class="pb-1">
                              Phone Number
                            </label>
                            <!-- Input group -->
                            <div class="input-group input-group-merge">
                              <div class="input-icon">
                                <i class="ri-mail-line color-primary"></i>
                              </div>
                              <input id="phone" type="number" class="form-control" placeholder="Phone" required visi />
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <!-- Label -->
                            <label class="pb-1">
                              Email Id
                            </label>
                            <!-- Input group -->
                            <div class="input-group input-group-merge">
                              <div class="input-icon">
                                <i class="ri-mail-line color-primary"></i>
                              </div>
                              <input id="email" type="email" class="form-control" placeholder="name.university@address.com"
                                required />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <!-- Label -->
                            <label class="pb-1">
                              Password
                            </label>
                            <!-- Input group -->
                            <div class="input-group input-group-merge">
                              <div class="input-icon">

                              </div>
                              <input  type="password" id= "firstPassword" class="form-control" placeholder="password" />
                            </div>
                          </div>
                          <div>
                          </div>
                        </div>

                        <div id="second_email" class="displayNone">
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <!-- Label -->
                                <label class="pb-1">
                                  Second Phone Number
                                </label>
                                <!-- Input group -->
                                <div class="input-group input-group-merge">
                                  <div class="input-icon">
                                    <i class="ri-mail-line color-primary"></i>
                                  </div>
                                  <input id="scphone" type="number" class="form-control" placeholder="Phone" required
                                    visi />
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <!-- Label -->
                                <label class="pb-1">
                                  Second Email Id
                                </label>
                                <!-- Input group -->
                                <div class="input-group input-group-merge">
                                  <div class="input-icon">
                                    <i class="ri-mail-line color-primary"></i>
                                  </div>
                                  <input id="scemail" type="email" class="form-control"
                                    placeholder="name.university@address.com" required />
                                </div>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <!-- Label -->
                                <label class="pb-1">
                                  Second password
                                </label>
                                <!-- Input group -->
                                <div class="input-group input-group-merge">
                                  <div class="input-icon">

                                  </div>
                                  <input  type="password" id='secondPassword' class="form-control" placeholder="password"
                                    required />
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div>

                        </div>






                        <div class="">

                          <!-- Submit -->
                          <button onclick="contactForm(event)"
                            class="btn btn-lg btn-block solid-btn-login border-radius mt-4 mb-3">
                            Sign up
                          </button>
                          <div>
                            <div>

                              <!-- Link -->
                              <div class="text-center pb-20">
                                <small class="text-muted text-center">
                                  Already have an account?
                                  <a href="#">Log in</a>.
                                </small>
                              </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- ======================================
    ********* Page Wrapper Area End ***********
    ======================================= -->

  <!-- JS here -->


  <script src="js/plugins.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
    crossorigin="anonymous"></script>
  <!-- Custom js-->
  <script src="js/main.js"></script>
  <script>
   


      // var collegeArr = []

      function showHideInfo() {
        var data_count = $('#checkbox-form').find("input[name='universities']:checked").length;
        // collegeArr.push($('#checkbox-form').find("input[name='universities']:checked").val())
        
        if (data_count == 2) {
          document.getElementById('second_email').style.display = 'block ';
        } else {
          document.getElementById('second_email').style.display = 'none';
        }
      }

      function contactForm(event) {
        event.preventDefault()

        var firstname = $('#fname').val();
        var sname = $('#sname').val();
        var fname = $('#fname').val();
        
        var email = $('#email').val();
        var phone = $('#phone').val();
        var scemail = $('#scemail').val();
        var scphone = $('#scphone').val();
        var firstPassword = $('#firstPassword').val();
        var secondPassword = $('#secondPassword').val();
        var institution = $('#institution').val();
        var subdomain = $("input[name='universities']:checked").map(function() {
            return this.value;
            
        }).get().join(', ')



        var datastring = 'firstname=' + firstname + '&email=' + email + '&mobile=' + phone + '&sname=' +
          sname + '&fname=' + fname+ '&phone=' + phone + '&institution=' + institution + '&subdomain=' + subdomain + '&scemail=' + scemail +
          '&scphone=' + scphone + '&firstPassword=' + firstPassword + '&secondPassword=' + secondPassword;


          if (firstname === '' || email === '' || phone === '') {
            alertify.error('Please fill the required fields.');
            $('#loader-img3').css("display", "none");

     }
   
        
        else {
          $.ajax({
            type: 'POST',
            url: 'ajax.php',
            cache: false,
            data: datastring,
            success: function (html) {


              swal("Thank you for the submission", "One of our agents will reach out to you shortly", "success");


              document.getElementById('fname').value = '';
              document.getElementById('lstname').value = '';
              document.getElementById('sname').value = '';
              document.getElementById('phone').value = '';
              document.getElementById('email').value = '';
              document.getElementById('firstPassword').value = '';
              document.getElementById('secondPassword').value = '';
              document.getElementById('institution').value = '';
              document.getElementById('scemail').value = '';
              document.getElementById('scphone').value = '';




            }
          })

        }

      }

  </script>
</body>

</html>