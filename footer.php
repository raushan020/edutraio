<div style="background:#eee">
  <div class="max-w-7xl mx-auto pt-16 px-4 sm:px-6 lg:px-8">
    <div class="bg-gray-700 rounded-lg shadow-xl overflow-hidden lg:grid lg:grid-cols-2 lg:gap-4">
      <div class="pt-10 pb-12 px-6 sm:pt-16 sm:px-16 lg:py-16 lg:pr-0 xl:py-20 xl:px-20">
        <div class="lg:self-center">
          <h2 class="text-3xl font-bold text-white sm:text-4xl mt-4">
            <span class="block">Ready to dive in?</span>
            <span class="block">Start your free trial today.</span>
          </h2>
          <p class="mt-4 text-lg leading-6 text-indigo-200 text-white">The small insights that will bring the big change in your business landscape. Get the insights
            that bring growth in your performance.</p>
          <a href="#" class="mt-8 bg-white border border-transparent  shadow px-5 py-3 inline-flex items-center text-base font-medium text-indigo-600 hover:bg-indigo-50">Sign up for free</a>
        </div>
      </div>
      <div class="-mt-6 aspect-w-5 aspect-h-3 md:aspect-w-2 md:aspect-h-1">
        <img class="transform translate-x-6 translate-y-6 rounded-md object-cover object-left-top sm:translate-x-16 lg:translate-y-20" src="img/crm-1.jpg" alt="App screenshot">
      </div>
    </div>
  </div>
</div>
  <!-- ##### Footer Area Start ##### -->
        <footer class="footer-area bg-img">
            <!-- style="background-image: url(img/footer-bg.png);" -->

            <!-- ##### Contact Area Start ##### -->
            <div class="contact_us_area" id="contact" style="background: #eee;">
                <div class="container">
                    <div>
                    <div class="call-us-sec">
                        <div class="row align-items-center">
                            <div class="col-12 col-lg-9">
                                <div class="text-left">
                                    <h3 class=" fadeInUp" data-wow-delay="0.3s">So what is next?</h3>
                                    <h2 class="bold w-text-1 mb-0">Are You Ready? Let’s Begin!</h2>
                                </div>
                            </div>
                            <div class="col-12 col-lg-3">
                                <a class="btn dream-btn-2 btn-1 btn-custm fadeInUp" data-wow-delay="0.6s"
                                    href="##">Read More</a>
                            </div>
                        </div>
                    </div>
                    </div>

                </div>
            </div>
            <!-- ##### Contact Area End ##### -->

            <div class="footer-content-area spec">
                <div class="container">
                    <div class="row ">
                        <div class="col-12 col-lg-4 col-md-6">
                            <div class="footer-copywrite-info">
                                <!-- Copywrite -->
                                <div class="copywrite_text fadeInUp" data-wow-delay="0.2s">
                                    <div class="footer-logo">
                                        <a href="##"><img src="img/mainimg/logo.png" alt="logo">
                                        </a>
                                    </div>
                                    <p class="text-edited" style="color:black !important">B-136, Sector-2, Noida</p>
                                    
                                        <p class="text-edited" style="color:black !important">support@edutratech.com</p>
                                   
                                     
                                        <p class="text-edited" style="color:black !important">+91 9911331937</p>
                                   
                                </div>
                                <!-- Social Icon -->
                                <!-- <div class="footer-social-info fadeInUp" data-wow-delay="0.4s">
                                    <a href="##"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="##"> <i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="##"><i class="fa fa-google-plus"
                                            aria-hidden="true"></i></a>
                                    <a href="##"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    <a href="##"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                </div> -->
                            </div>
                        </div>

                        <div class="col-12 col-lg-3 col-md-6">
                            <div class="contact_info_area d-sm-flex justify-content-between">
                                <!-- Content Info -->
                                <div class="contact_info mt-x text-center fadeInUp" data-wow-delay="0.3s">
                                    <h5 style="color:black !important">Products</h5>
                                    <a class="text-decoration" href="#">
                                        <p style="color:#086AD8 !important">Lead Management</p>
                                    </a>
                                    <a class="text-decoration" href="#">
                                        <p style="color:#086AD8 !important">Marketing Automation</p>
                                    </a>
                                    <a class="text-decoration" href="#">
                                        <p style="color:#086AD8 !important">Drip Marketing</p>
                                    </a>
                                    <a class="text-decoration" href="#">
                                        <p style="color:#086AD8 !important">Sales Execution</p>
                                    </a>
                                  <!--   <a class="text-decoration" href="#">
                                        <p>Technology Privacy</p>
                                    </a>
                                    <a class="text-decoration" href="#">
                                        <p>Developer Agreement</p>
                                    </a> -->
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-lg-2 col-md-6 ">
                            <!-- Content Info -->
                            <div class="contact_info_area d-sm-flex justify-content-between">
                                <div class="contact_info mt-s text-center fadeInUp" data-wow-delay="0.2s">
                                    <h5 style="color:black !important">Quick Links</h5>
                                    <a class="text-decoration" href="#">
                                        <p style="color:#086AD8 !important">Pricing Info</p>
                                    </a>
                                    <a class="text-decoration" href="#">
                                        <p style="color:#086AD8 !important">Documentation</p>
                                    </a>
                                    <a class="text-decoration" href="privacy-policy.php">
                                        <p style="color:#086AD8 !important">Privacy Policy</p>
                                    </a>
                                    <a class="text-decoration" href="terms.php">
                                        <p style="color:#086AD8 !important">Terms of Service</p>
                                    </a>
                                    <!-- <a class="text-decoration" href="#">
                                        <p>Connect</p>
                                    </a> -->
                                </div>
                            </div>
                        </div>


                        <div class="col-12 col-lg-3 col-md-6 ">
                            <div class="contact_info_area d-sm-flex justify-content-between">
                                <!-- Content Info -->
                                <div class="contact_info mt-s text-center fadeInUp" data-wow-delay="0.4s">
                                    <h5 style="color:black !important">Company</h5>
                                    <a class="text-decoration" href="#">
                                        <p style="color:#086AD8 !important">About</p>
                                    </a>
                                    <a class="text-decoration" href="#">
                                        <p style="color:#086AD8 !important">Meet the Team</p>
                                    </a>
                                    <a class="text-decoration" href="#">
                                        <p style="color:#086AD8 !important">Contact Us</p>
                                    </a>
                                   
                                </div>
                            </div>
                        </div>
                       <!--  <div class="col-sm-12">
                            <p class="coprightfooter">&copy; 2021 Edge Technosoft Pvt Ltd. All Rights Reserved</p>
                        </div> -->
                        <!-- This example requires Tailwind CSS v2.0+ -->
<!-- <div class="relative">
  <div class="absolute inset-0 flex items-center" aria-hidden="true">
    <div class="w-full border-t border-gray-300"></div>
  </div>
  <div class="relative flex justify-center">
    <span class="px-2 bg-white text-sm text-gray-500">
     &copy; 2021 Edutra Technologies LLP. All Rights Reserved
    </span>
  </div>
</div> -->


<div class="row align-items-center mt-5 pt-2">
    <div class="col-md-12 col-sm-12 text-center">
        <p class="text-edited">© 2021 Edutra Technologies LLP. All Rights Reserved</p>
        <p class="text-edited">Made With <i class="fa fa-heart text-danger"></i> in India</p>
          <!-- Social Icon -->
          <!-- <div class="footer-social-info fadeInUp" data-wow-delay="0.4s">
            <a href="##"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a href="##"> <i class="fa fa-twitter" aria-hidden="true"></i></a>
            <a href="##"><i class="fa fa-google-plus"
                    aria-hidden="true"></i></a>
            <a href="##"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            <a href="##"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
        </div> -->
    </div>

</div>

                    </div>
                </div>

            </div>
        </footer>
        <!-- ##### Footer Area End ##### -->
    </div>

    <!-- ########## All JS ########## -->
    <!-- jQuery js -->
    <script src="js/jquery.min.js"></script>
    <!-- Popper js -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <!-- All Plugins js -->
    <script src="js/plugins.js"></script>
    <!-- Parallax js -->
    <script src="js/dzsparallaxer.js"></script>

    <script src="js/jquery.syotimer.min.js"></script>

    <!-- script js -->
    <script src="js/script.js"></script>

    
<script>

$(function() {
$( ".calendar" ).datepicker({
    dateFormat: 'dd/mm/yy',
    firstDay: 1
});

$(document).on('click', '.date-picker .input', function(e){
    var $me = $(this),
            $parent = $me.parents('.date-picker');
    $parent.toggleClass('open');
});


$(".calendar").on("change",function(){
    var $me = $(this),
            $selected = $me.val(),
            $parent = $me.parents('.date-picker');
    $parent.find('.result').children('span').html($selected);
});

$('.datafst .ui-state-default').click (function(){
    $('.fsttime').fadeToggle('');
});

$('.datanext .ui-state-default').click (function(){
    $('.nexttime').fadeToggle('');
});


});

</script>

</body>

</html>