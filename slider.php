<style type="">
	.col-md-3 {
  display: inline-block;
  margin-left: -4px;
}
.col-md-3 img {
  width: auto;
  height: auto;
}
body .carousel-indicators li {
  background-color: transparent;
}
body .carousel-control-prev-icon,
body .carousel-control-next-icon {
  background-color: transparent;
}
body .no-padding {
  padding-left: 0;
  padding-right: 0;
}

</style>
<section id="advantageSection" style="background: #fff">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="heading text-center"> Over 10,000 paying customers trust Edutra CRM to access more than 75,000 hours of content every month.</h1>
      </div>
       <div class="col-12 mt-4">
       	<div id="demo" class="carousel slide" data-ride="carousel">

  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>
  
  <!-- The slideshow -->
  <div class="container carousel-inner no-padding">
    <div class="carousel-item active">
      <div class="col-xs-3 col-sm-3 col-md-3">
        <img src="img/Chandigarh-University-color.png" class="uni-img" alt="">
      </div>    
      <div class="col-xs-3 col-sm-3 col-md-3">
         <img src="img/cv-color.png" alt="" class="uni-img">
      </div>   
      <div class="col-xs-3 col-sm-3 col-md-3">
          <img src="img/itm_logo_hover-removebg-preview.png" alt="" class="uni-img">
      </div>   
      <div class="col-xs-3 col-sm-3 col-md-3">
         <img src="img/Jaipur-National-University-color.png" alt="" class="uni-img">
      </div>   
    </div>
    <div class="carousel-item">
      <div class="col-xs-3 col-sm-3 col-md-3">
        <img src="img/Lingayas-Vidyapeeth-color.png" alt="" class="uni-img">
      </div>    
      <div class="col-xs-3 col-sm-3 col-md-3">
        <img src="img/Shri-venkateshwar-University-color.png" alt="" class="uni-img">
      </div>   
      <div class="col-xs-3 col-sm-3 col-md-3">
        <img src="img/Chandigarh-University-color.png" class="uni-img" alt="">
      </div>   
      <div class="col-xs-3 col-sm-3 col-md-3">
        <img src="img/itm_logo_hover-removebg-preview.png" alt="" class="uni-img">
      </div>  
    </div>
    <!-- <div class="carousel-item">
      <div class="col-xs-3 col-sm-3 col-md-3">
        <img src="img/Shri-venkateshwar-University-color.png" alt="" class="uni-img">
      </div>    
      <div class="col-xs-3 col-sm-3 col-md-3">
        <img src="img/itm_logo_hover-removebg-preview.png" alt="" class="uni-img">
      </div>   
      
      <div class="col-xs-3 col-sm-3 col-md-3">
        <img src="img/Lingayas-Vidyapeeth-color.png" alt="" class="uni-img">
      </div>  
    </div> -->
  </div>
  
  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
       </div>
      </div>
  </div>
</section>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

  